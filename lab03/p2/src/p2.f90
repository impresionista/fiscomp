program lab02_p2

  use precision_interface
  use random
  use mzranmod
  use mtmod,      only: grnd, sgrnd

  use p2_GlobalVariables
  use p2_Functions

  implicit none
  integer   ::  i, j, k, seed, ii, dir
  real(rp)  ::  dummy_pos(3)


  call OpenFiles()

! Caminata en 2D --------------------------------------------------------------
  call WalkParameters()
  call WriteLog(title="Caminata en 2D")

  call PrepareWalk_2D

  do j = 1, Nwalks, 1
    seed = j

    ! ran2 -------------------------------------------------
    call InitialData_2D
    Walk2D_ran2: do i = 1, Nsteps, 1
      ii  = aint(100*ran2(seed))/25
      dir = 1+ii/2
      pos2(dir) = pos2(dir)+(-1)**ii
      if ( mod(i,Nsamp) == 0 ) then
        k = i/Nsamp
        disp_r2(k) = disp_r2(k) + norm2(pos2)**2
        ii = ccuad(pos2)
        cuad_count_r2(k,ii) = cuad_count_r2(k,ii) + 1
      end if
    end do Walk2D_ran2

    ! MZran -----------------------------------------------
    call InitialData_2D
    Walk2D_mzran: do i = 1, Nsteps, 1
      ii  = aint(100*rmzran())/25
      dir = 1+ii/2
      pos2(dir) = pos2(dir)+(-1)**ii
      if ( mod(i,Nsamp) == 0 ) then
        k = i/Nsamp
        disp_mz(k) = disp_mz(k) + norm2(pos2)**2
        ii = ccuad(pos2)
        cuad_count_mz(k,ii) = cuad_count_mz(k,ii) + 1
      end if
    end do Walk2D_mzran

    ! Mersenne Twister ------------------------------------
    call InitialData_2D
    Walk2D_mt: do i = 1, Nsteps, 1
      ii  = aint(100*grnd())/25
      dir = 1+ii/2
      pos2(dir) = pos2(dir)+(-1)**ii
      if ( mod(i,Nsamp) == 0 ) then
        k = i/Nsamp
        disp_mt(k) = disp_mt(k) + norm2(pos2)**2
        ii = ccuad(pos2)
        cuad_count_mt(k,ii) = cuad_count_mt(k,ii) + 1
      end if
    end do Walk2D_mt

  end do

  disp_r2 = disp_r2/real(Nwalks, rp)
  disp_mz = disp_mz/real(Nwalks, rp)
  disp_mt = disp_mt/real(Nwalks, rp)
  do i = 1, Nsteps/Nsamp
    write(file_msd2d, *) i*Nsamp, disp_r2(i), disp_mz(i), disp_mt(i)
  end do

  do i = 1, ubound(cuad_count_r2, dim=1)
    write(file_cuad, *) i*Nsamp, real(cuad_count_r2(i, :),rp)/(real(Nwalks, rp))
  end do
  write(file_cuad, *) ""
  write(file_cuad, *) ""
  do i = 1, ubound(cuad_count_mz, dim=1)
    write(file_cuad, *) i*Nsamp, real(cuad_count_mz(i, :),rp)/(real(Nwalks, rp))
  end do
  write(file_cuad, *) ""
  write(file_cuad, *) ""
  do i = 1, ubound(cuad_count_mt, dim=1)
    write(file_cuad, *) i*Nsamp, real(cuad_count_mt(i, :),rp)/(real(Nwalks, rp))
  end do

  call CleanWalk()



! Caminata en 3D --------------------------------------------------------------
  call WalkParameters()
  call WriteLog(title="Caminata en 3D")

  call PrepareWalk_3D

  do j = 1, Nwalks, 1
    seed = j

    ! ran2 ------------------------------------------------
    call InitialData_3D
    Walk3D_ran2: do i = 1, Nsteps, 1
      ii  = aint(300*ran2(seed))/50
      dir = 1+ii/2
      pos3(dir) = pos3(dir)+(-1)**ii
      if ( mod(i,Nsamp) == 0 ) then
        k = i/Nsamp
        disp_r2(k) = disp_r2(k) + norm2(pos3)**2
        ii = cocta(pos3)
        octa_count_r2(k,ii) = octa_count_r2(k,ii) + 1
      end if
    end do Walk3D_ran2

    ! MZran -----------------------------------------------
    call InitialData_3D
    Walk3D_mzran: do i = 1, Nsteps, 1
      ii  = aint(300*ran2(seed))/50
      dir = 1+ii/2
      pos3(dir) = pos3(dir)+(-1)**ii
      if ( mod(i,Nsamp) == 0 ) then
        k = i/Nsamp
        disp_mz(k) = disp_mz(k) + norm2(pos3)**2
        ii = cocta(pos3)
        octa_count_mz(k,ii) = octa_count_mz(k,ii) + 1
      end if
    end do Walk3D_mzran

    ! Mersenne Twister ------------------------------------
    call InitialData_3D
    Walk3D_mt: do i = 1, Nsteps, 1
      ii  = aint(300*ran2(seed))/50
      dir = 1+ii/2
      pos3(dir) = pos3(dir)+(-1)**ii
      if ( mod(i,Nsamp) == 0 ) then
        k = i/Nsamp
        disp_mt(k) = disp_mt(k) + norm2(pos3)**2
        ii = cocta(pos3)
        octa_count_mt(k,ii) = octa_count_mt(k,ii) + 1
      end if
    end do Walk3D_mt

  end do


  disp_r2 = disp_r2/real(Nwalks, rp)
  disp_mz = disp_mz/real(Nwalks, rp)
  disp_mt = disp_mt/real(Nwalks, rp)
  do i = 1, Nsteps/Nsamp
    write(file_msd3d, *) i*Nsamp, disp_r2(i), disp_mz(i), disp_mt(i)
  end do

  do i = 1, ubound(octa_count_r2, dim=1)
    write(file_octa, *) i*Nsamp, real(octa_count_r2(i, :),rp)/(real(Nwalks, rp))
  end do
  write(file_octa, *) ""
  write(file_octa, *) ""
  do i = 1, ubound(octa_count_mz, dim=1)
    write(file_octa, *) i*Nsamp, real(octa_count_mz(i, :),rp)/(real(Nwalks, rp))
  end do
  write(file_octa, *) ""
  write(file_octa, *) ""
  do i = 1, ubound(octa_count_mt, dim=1)
    write(file_octa, *) i*Nsamp, real(octa_count_mt(i, :),rp)/(real(Nwalks, rp))
  end do


  call CleanWalk()

  call CloseFiles()

end program lab02_p2
