module p2_Functions

  use precision_interface

  implicit none

  private
  public ccuad, cocta, msd


contains

  function ccuad(pp)
  ! calcula el cuadrante donde se ubica el punto pp=(x,y)
  implicit none
  integer               ::  ic, ccuad
  real(rp), intent(in)  ::  pp(2)
    if ( (pp(1)>0) .and. (pp(2)>=0) ) then
      ic  = 1
    else if ( (pp(1)<=0) .and. (pp(2)>0) ) then
      ic  = 2
    else if ( (pp(1)<0) .and. (pp(2)<=0) ) then
      ic  = 3
    else if ( (pp(1)>=0) .and. (pp(2)<0) ) then
      ic  = 4
    else
      ic  = 0
    end if
    ccuad = ic
  end function ccuad


  function cocta(pp)
  ! calcula el octante donde se ubica el punto pp=(x,y,z)
  implicit none
  integer               ::  ic, cocta
  real(rp), intent(in)  ::  pp(3)
    if      ( (pp(1)>0) .and. (pp(2)>=0) .and. (pp(3)>=0) ) then
      ic  = 1
    else if ( (pp(1)<=0) .and. (pp(2)>0) .and. (pp(3)>0) ) then
      ic  = 2
    else if ( (pp(1)<0) .and. (pp(2)<=0) .and. (pp(3)>=0) ) then
      ic  = 3
    else if ( (pp(1)>=0) .and. (pp(2)<0) .and. (pp(3)>0) ) then
      ic  = 4
    else if ( (pp(1)>=0) .and. (pp(2)<0) .and. (pp(3)<=0) ) then
      ic  = 5
    else if ( (pp(1)<0) .and. (pp(2)<=0) .and. (pp(3)<0) ) then
      ic  = 6
    else if ( (pp(1)<=0) .and. (pp(2)>0) .and. (pp(3)<=0) ) then
      ic  = 7
    else if ( (pp(1)>0) .and. (pp(2)>=0) .and. (pp(3)<0) ) then
      ic  = 8
    else
      ic  = 0
    end if
    cocta = ic
  end function cocta


  function msd(xx,NN)
  ! Calcula el desplazamiento cuadrático medio (mean squared displacement)
  ! para N pasos cuadráticos acumulados en xx.
  implicit none
  integer, intent(in)   :: NN
  real(rp), intent(in)  :: xx
  real :: msd
    msd = xx/real(NN,rp)
  end function msd

end module p2_Functions
