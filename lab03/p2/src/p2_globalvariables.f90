module p2_GlobalVariables

  use precision_interface

  implicit none
  integer(ip)   ::  Nsteps, Nwalks, Nsamp
  real(rp)      ::  pos2(2), pos3(3)
  real(rp), allocatable :: disp_r2(:), disp_mz(:), disp_mt(:)
  integer(ip), allocatable  :: cuad_count_r2(:,:), octa_count_r2(:,:),        &
                               cuad_count_mz(:,:), octa_count_mz(:,:),        &
                               cuad_count_mt(:,:), octa_count_mt(:,:)

  integer, parameter :: logfile=100, file_cuad=101, file_octa=102,            &
                        file_msd2d=103, file_msd3d=104

contains

! Subrutinas de manejo de archivos --------------------------------------------
  subroutine OpenFiles()
    implicit none
    open(unit=logfile,    file='./p2.log',                position='append')
    open(unit=file_cuad,  file='./output/cuadrants.dat',  status='replace')
    open(unit=file_octa,  file='./output/octants.dat',    status='replace')
    open(unit=file_msd2d, file='./output/msd_2d.dat',     status='replace')
    open(unit=file_msd3d, file='./output/msd_3d.dat',     status='replace')
  end subroutine OpenFiles

  subroutine CloseFiles()
    implicit none
    close(logfile)
    close(file_cuad)
    close(file_octa)
    close(file_msd2d)
    close(file_msd3d)
  end subroutine CloseFiles

  subroutine WriteLog(title)
    implicit none
    character(8)  :: date
    character(10) :: time
    character(*), OPTIONAL :: title
    call date_and_time(DATE=date,TIME=time)
    write(logfile, *) title
    write(logfile, *) "Fecha: ", date, " | ", "Hora: ", time
    write(logfile, *) "Cantidad de pasos por caminata: ", Nsteps
    write(logfile, *) "Cantidad de caminatas: ", Nwalks
    write(logfile, *) "Cantidad de muestras de c/caminata: ", Nsamp
    write(logfile, *) ""
  end subroutine WriteLog



! Subrutinas para caminatas ---------------------------------------------------
  subroutine WalkParameters()
  ! ---------------------------------------------------------------------------
  ! Define parámetros de la caminata.
  ! ---------------------------------------------------------------------------
    implicit none
    integer   ::  samp_size
    Nsteps  = 10**2             !> Número de pasos para la caminata
    Nwalks  = 10**6             !> Número de repeticiones de la caminata
    Nsamp   = 1                 !> Número de muestras para la caminata
    samp_size   = Nsteps/Nsamp
    allocate(disp_r2(samp_size))
    allocate(disp_mz(samp_size))
    allocate(disp_mt(samp_size))
    allocate(cuad_count_r2(samp_size,0:4), octa_count_r2(samp_size,0:8))
    allocate(cuad_count_mz(samp_size,0:4), octa_count_mz(samp_size,0:8))
    allocate(cuad_count_mt(samp_size,0:4), octa_count_mt(samp_size,0:8))
    return
  end subroutine WalkParameters

  subroutine CleanWalk()
    implicit none
    deallocate(disp_r2, disp_mz, disp_mt)
    deallocate(cuad_count_r2, octa_count_r2)
    deallocate(cuad_count_mz, octa_count_mz)
    deallocate(cuad_count_mt, octa_count_mt)
  end subroutine CleanWalk


! Subruttinas en 2D -----------------------------------------------------------
  subroutine InitialData_2D()
  ! ---------------------------------------------------------------------------
  ! Define los valores iniciales de las variables.
  ! ---------------------------------------------------------------------------
    implicit none
    pos2    = [0.0_rp, 0.0_rp]  !> Posición inicial de la caminata en (x,y)
    return
  end subroutine InitialData_2D

  subroutine PrepareWalk_2D()
    implicit none
    disp_r2   = 0.0_rp
    disp_mz   = 0.0_rp
    disp_mt   = 0.0_rp
    cuad_count_r2 = 0.0_rp
    cuad_count_mz = 0.0_rp
    cuad_count_mt = 0.0_rp
    return
  end subroutine PrepareWalk_2D


! Subruttinas en 3D -----------------------------------------------------------
  subroutine InitialData_3D()
  ! ---------------------------------------------------------------------------
  ! Define los valores iniciales de las variables.
  ! ---------------------------------------------------------------------------
    implicit none
    pos3    = [0.0_rp, 0.0_rp, 0.0_rp]  !> Posición inicial de la caminata en (x,y,z)
    return
  end subroutine InitialData_3D

  subroutine PrepareWalk_3D()
    implicit none
    disp_r2   = 0.0_rp
    disp_mz   = 0.0_rp
    disp_mt   = 0.0_rp
    octa_count_r2 = 0.0_rp
    octa_count_mz = 0.0_rp
    octa_count_mt = 0.0_rp
    return
  end subroutine PrepareWalk_3D


end module p2_GlobalVariables
