\babel@toc {spanish}{}\relax 
\contentsline {chapter}{\numberline {1}Compilación y ejecución}{1}{chapter.1}%
\contentsline {chapter}{\numberline {2}Breve introducción teórica}{2}{chapter.2}%
\contentsline {section}{\numberline {2.1}Algoritmos usados}{2}{section.2.1}%
\contentsline {section}{\numberline {2.2}Implementación computacional}{2}{section.2.2}%
\contentsline {chapter}{\numberline {3}Problema 2: Caminatas al azar}{3}{chapter.3}%
\contentsline {section}{\numberline {3.1}Descripción teórica}{3}{section.3.1}%
\contentsline {section}{\numberline {3.2}Análisis}{3}{section.3.2}%
\contentsline {chapter}{\numberline {4}Problema 3: Integración de Monte Carlo}{10}{chapter.4}%
\contentsline {section}{\numberline {4.1}Descripción teórica}{10}{section.4.1}%
\contentsline {section}{\numberline {4.2}Análisis}{10}{section.4.2}%
