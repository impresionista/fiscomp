#!/bin/bash
# Autor: Marcos Ferradas

################################################################################
# VARIABLES A DEFINIR
################################################################################

# Programa a compilar SIN EXTENSION
PROGRAM=p3

PROGRAM_DIR=$(dirname "$0")
SRC_DIR="$PROGRAM_DIR/src"        #> Codigo fuente
BIN_DIR="$PROGRAM_DIR/bin"        #> Librerias compiladas
LIB_DIR="$PROGRAM_DIR/lib"        #> Librerias generales
LIB_DIR_LOCAL="$SRC_DIR"          #> Librerias locales

# Lista de módulos a usar en orden y separados por espacios o con salto de linea
declare -a MODULES=(
                    "mod_constantes"
                    "mod_random"
                    "mod_mt"
                    "mod_mzran"
                    "mod_integracion"
                    )

# Lista de módulos locales a usar en orden y separados por espacios o con salto
# de linea
declare -a LOCAL_MODULES=(
                          "precision_interface"
                          "p3_variables"
                          "p3_subroutines"
                          )

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# Elegir compilación.
# Todas las compilaciones tienen flags de aviso para división por cero,
# operaciones de punto flotante inválidas y errores de overflow y underflow.
# Además usan optimización agresiva (-O3) (ver opcion -ffrontend-optimize de
# man-page de gfortran).
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# OPCIONES:
#   1) Compilación normal
#   2) Compilación usando fftw3
#   3) Compilación normal con opciones de DEBUGGING
#   4) Compilación usando fftw3 con opciones de DEBUGGING
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
COMPILATION_MODE=1
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# ¿Chequeos extras? (0=>NO, 1=>SI)
FCHECK_FLAG=0
# Descomentar flags que se quieran usar
declare -a FCHECKS=(
                    # "do"
                    # "bounds"
                    # "recursion"
                    # "array-temps"
                    # "mem"
                    # "pointer"
                    )
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# CHECKS y FLAGS extras
# -Wall
# -g
# -Wuninitialized
EXTRA="-g"
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


################################################################################
#~~~~~~~~~~~~~~~~~~~~~~~ NO EDITAR DE ACA EN ADELANTE ~~~~~~~~~~~~~~~~~~~~~~~~~#
################################################################################

function buildModLists {

  # Creo string que tiene la lista de modulos locales a compilar
  MOD_LIST=""
  for i in "${MODULES[@]}"
  do
    MOD_LIST=" ${MOD_LIST}${LIB_DIR}/${i}.f90 "
  done

  # Creo string que tiene la lista de modulos generales a compilar
  LOCAL_MOD_LIST=""
  for i in "${LOCAL_MODULES[@]}"
  do
    LOCAL_MOD_LIST=" ${LOCAL_MOD_LIST}${LIB_DIR_LOCAL}/${i}.f90 "
  done

  return
}


function buildCheckLists {
  case $1 in
    0)  FCHECK=""
        ;;
    1)  [[ ${#FCHECKS[@]} -eq 0 ]] &&
          echo "-----------------------------------------------------------------------"&&
          echo -e "WARNING: No se especificaron Chequeos." &&
          echo "-----------------------------------------------------------------------"&&
          FCHECK_FLAG=0 &&
          return
        echo "-----------------------------------------------------------------------"
        echo "Compilando usando los chequeos:" "${FCHECKS[@]}"
        echo "-----------------------------------------------------------------------"
        FCHECKS_LIST="${FCHECKS[0]}"
        for i in "${FCHECKS[@]:1}"
        do
          FCHECKS_LIST="$FCHECKS_LIST,$i"
        done
        FCHECK=" -fcheck=${FCHECKS_LIST} "
        ;;
    *)  echo -e "Opción de compilación incorrecta para chequeos (FCHECK_FLAG)"
        exit 0
        ;;
  esac
  return
}


# Defino función que compila
function pickCompileMethod {

  # Selecciona tipo de compilación
  case $1 in
    1)  MESSAGE="Compilando de forma habitual"
        COMPILE="gfortran -ffpe-trap=zero,invalid,overflow -O3"
        ;;
    2)  MESSAGE="Compilando con fftw3"
        COMPILE="gfortran -ffpe-trap=zero,invalid,overflow -O3 -l fftw3"
        ;;
    3)  MESSAGE="DEBUGGING: Compilando de forma habitual"
        COMPILE="gfortran -ffpe-trap=zero,invalid,overflow -fcheck=bounds -fbacktrace -Wextra"
        ;;
    4)  MESSAGE="DEBUGGING: Compilando con fftw3"
        COMPILE="gfortran -ffpe-trap=zero,invalid,overflow -l fftw3 -fcheck=bounds -fbacktrace"
        ;;
    *)  echo -e "Opción de compilación incorrecta"
        exit 0
        ;;
  esac

  echo "-----------------------------------------------------------------------"
  echo "$MESSAGE"
  echo "-----------------------------------------------------------------------"

  return
}


function compile {
  PROGRAM_IN="$SRC_DIR/${PROGRAM}.f90"
  PROGRAM_OUT="-o ${PROGRAM}.x"
  $COMPILE $EXTRA $FCHECK -J $BIN_DIR -I $BIN_DIR "./lib/mod_precision.f90" "./src/precision_interface.f90" $MOD_LIST $LOCAL_MOD_LIST $PROGRAM_IN $PROGRAM_OUT
  return
}


# -----------------------------------------------------------------------------
buildCheckLists $FCHECK_FLAG
buildModLists
pickCompileMethod $COMPILATION_MODE
compile
# -----------------------------------------------------------------------------
