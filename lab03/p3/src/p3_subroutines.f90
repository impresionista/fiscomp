module p3_subroutines

  use precision_interface

  implicit none

  private
  public PrintLog, WriteIntegration, exactError, WriteRuntime, runtime

contains

  subroutine PrintLog(file, xmin, xmax, nn, step, exact)
    implicit none
    real(rp), intent(in)    ::  xmin, xmax, exact
    integer(ip), intent(in) ::  file, NN, step
    character(len=10)       ::  date, time
    character(len=32)       ::  fmt_1, fmt_2

    fmt_1 = '(2X,f10.3,X,A)'
    fmt_2 = '(2X,I10,X,A)'

    call date_and_time(date, time)
    write(file,*) 'Fecha de corrida: ', date, time
    write(file,*) 'Parametros de corrida'
    write(file,fmt_1) xmin,   "!> Pto min del intervalo de integracion"
    write(file,fmt_1) xmax,   "!> Pto max del intervalo de integracion"
    write(file,fmt_2) NN,     "!> Puntos maximos para integrar"
    write(file,fmt_2) step,   "!> Intervalo de muestreo para integracion"
    write(file,fmt_1) exact,  "!> Valor exacto de la integral"
    write(file,*) "--------------------------------------------------------------------------------"
  end subroutine PrintLog

  subroutine WriteIntegration(file, nstep, xx)
    implicit none
    integer(ip), intent(in) ::  file, nstep
    real(rp), intent(in)    ::  xx(:)
    character(len=32)       ::  fmt_, size_
      write(size_, '(I32)') ubound(xx, dim=1)
      fmt_ = "(I8,"//trim(adjustl(size_))//"(X,ES22.16))"
      write(file,fmt_) nstep, xx
  end subroutine WriteIntegration

  subroutine WriteRuntime(file, nstep, xx)
    implicit none
    integer(ip), intent(in) ::  file, nstep
    real(rp), intent(in)    ::  xx(:)
    character(len=32)       ::  fmt_, size_
      write(size_, '(I32)') ubound(xx, dim=1)
      fmt_ = "(I8,"//trim(adjustl(size_))//"(X,ES16.8))"
      write(file,fmt_) nstep, xx
  end subroutine WriteRuntime

  function exactError(xx, yy)
    implicit none
    real(rp), intent(in)    ::  xx, yy
    real(rp)                ::  exactError
    exactError  = abs(xx-yy)
  end function exactError

  function runtime(xx, yy)
    implicit none
    real(rp), intent(in)    ::  xx, yy
    real(rp)                :: runtime
    runtime  = abs(xx-yy)
  end function runtime

end module p3_subroutines
