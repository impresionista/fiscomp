program lab03_p3

  use precision_interface
  use p3_variables
  use p3_subroutines
  use integracion

  implicit none
  integer   ::  i, j, k

  ! Condiciones iniciales -----------------------------------------------------
  x_min = 0.0_rp        !> Pto min del intervalo de integracion
  x_max = 1.0_rp        !> Pto max del intervalo de integracion
  Neval = 10**5         !> Nro máximo de puntos para integrar
  step  = 10            !> Intervalo de muestreo para integracion
  exact = 1._rp/4._rp   !> Valor exacto de la integral
  ! ---------------------------------------------------------------------------

  call OpenFiles()
  call PrintLog(file=logfile, xmin=x_min, xmax=x_max, nn=Neval, step=step, exact=exact)

  do i = 10, Neval, step
    call cpu_time(rt1i)
    call integrate_mc(f=f, xmin=x_min, xmax=x_max, nptos=i, rand="mt", &
                      ss=f_inte_mt, err=f_err_mt)
    call cpu_time(rt1f)

    call cpu_time(rt2i)
    call integrate_mc(f=f, xmin=x_min, xmax=x_max, nptos=i, rand="mz", &
                      ss=f_inte_mz, err=f_err_mz)
    call cpu_time(rt2f)

    call cpu_time(rt3i)
    call integrate_mc_sampling(f=f, samp=f_int,samp_1=f_int_inv, nptos=i,     &
                                rand="mt", ss=f_inte_mt_samp, err=f_err_mt_samp)
    call cpu_time(rt3f)

    call cpu_time(rt4i)
    call integrate_mc_sampling(f=f, samp=f_int,samp_1=f_int_inv, nptos=i,     &
                                rand="mz", ss=f_inte_mz_samp, err=f_err_mz_samp)
    call cpu_time(rt4f)

    ee_mt = exactError(f_inte_mt, exact)
    ee_mz = exactError(f_inte_mz, exact)
    ee_mt_samp = exactError(f_inte_mt_samp, exact)
    ee_mz_samp = exactError(f_inte_mz_samp, exact)

    call WriteIntegration(file=file_inte, nstep=i,                            &
                          xx=[f_inte_mt, f_err_mt, ee_mt,                     &
                              f_inte_mz, f_err_mz, ee_mz,                     &
                              f_inte_mt_samp, f_err_mt_samp, ee_mt_samp,      &
                              f_inte_mz_samp, f_err_mz_samp, ee_mz_samp]      &
                          )

    rt1 = runtime(rt1f, rt1i)
    rt2 = runtime(rt2f, rt2i)
    rt3 = runtime(rt3f, rt3i)
    rt4 = runtime(rt4f, rt4i)

    call WriteRuntime(file=file_runtime, nstep=i, xx=[rt1, rt2, rt3, rt4])

  end do

  call CloseFiles()


contains

  function f(x)
    implicit none
    real(rp), intent(in)  ::  x
    integer(ip) ::  n
    real(rp)    ::  f
      n = 3_ip
      f = x**n
  end function f

  function f_int(x)
    implicit none
    real(rp), intent(in)  ::  x
    integer(ip) ::  n
    real(rp)    ::  f_int
      n = 2_ip
      f_int = real(n+1,rp)*x**n
  end function f_int

  function f_int_inv(x)
    implicit none
    real(rp), intent(in)  ::  x
    integer(ip) ::  n
    real(rp)    ::  f_int_inv, expo
      n = 2_ip
      expo  = 1._rp/real(n+1,rp)
      f_int_inv = x**expo
  end function f_int_inv

end program lab03_p3

