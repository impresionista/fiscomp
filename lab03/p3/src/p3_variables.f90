module p3_variables

  use precision_interface

  implicit none
  real(rp)    ::  x_min, x_max, exact, f_inte_mt, f_err_mt, f_inte_mz,        &
                  f_err_mz, f_inte_mt_samp, f_err_mt_samp, f_inte_mz_samp,    &
                  f_err_mz_samp, ee_mt, ee_mz, ee_mt_samp, ee_mz_samp,        &
                  rt1i, rt1f, rt2i, rt2f, rt3i, rt3f, rt4i, rt4f,             &
                  rt1, rt2, rt3, rt4
  integer(ip) ::  Neval, step
  integer(ip), parameter  ::  logfile=100, file_inte=101, file_runtime=102

contains

  subroutine OpenFiles()
    implicit none
    open(unit=logfile,      file='./p3.log',                position='append')
    open(unit=file_inte,    file='./output/integrated.dat', status='replace')
    open(unit=file_runtime, file='./output/runtime.dat',    status='replace')
  end subroutine OpenFiles

  subroutine CloseFiles()
    implicit none
    close(logfile)
    close(file_inte)
    close(file_runtime)
  end subroutine CloseFiles

end module p3_variables
