program lab02_p1

  use precision_interface
  use linalgebra
  use p1_GlobalVariables
  use p1_functions

  implicit none
  integer   ::  i, j
  real(rp)  ::  Ti, xi, cpu_t_start, cpu_t_stop,  alpha, alpha_cn, er_fw,     &
                er_bw, er_cn, er_check
  real(rp), allocatable   ::  dinf(:), dcent(:), dsup(:), B(:),               &
                              dinf_cn(:), dcent_cn(:), dsup_cn(:), B_cn(:)

  logical   ::  er_fw_done, er_bw_done, er_cn_done

  ! ---------------------------------------------------------------------------
  ! Defino condiciones iniciales y de borde
  t_0 = 0._rp
  x_0 = 0._rp; x_f = 1._rp
  ! Temperatuta en Kelvin
  T_0x  = 373.15_rp
  T_t0  = 273.15_rp;  T_tl  = 273.15_rp

  t_f = 3000._rp  !> Tiempo de corrida
  Nx  = 100       !> ptos para evaluacion espacial
  ht  = 0.3_rp    !> Ancho de paso temporal

  sampling  = 300 !> Cada cuantos punos escribo en disco
  ! ---------------------------------------------------------------------------


  call OpenFiles()
  call InitConditions()

  ! goto 10000
  ! Solucion analitica --------------------------------------------------------
    print*, "Calculo con solucion analitica"
    call cpu_time(cpu_t_start)

    TimeSweep_an: do j = 0, Nt, sampling
      time = t0_a + real(j,rp)*ht_a
      do i = 1, Nx, 1
        call HeatAnalitic(T_0x, 3000, time, x(i), Ti)
        write(file_heat_an,fmt_hmap) adimTime(-1, time), adimX(-1, x(i)), rescaleTemp(-1, Ti)
      end do
      write(file_heat_an,*) ""
    end do TimeSweep_an

    call cpu_time(cpu_t_stop)
    print *, "Tiempo de CPU:", abs(cpu_t_stop-cpu_t_start), "seg"
    print*, "--------------------------------------------------------"
  ! ---------------------------------------------------------------------------


  ! Forward -------------------------------------------------------------------
    print*, "Calculo con metodo explicito (forward)"

    ! Creo arreglo de datos de temperatura al tiempo ti
    call InitBar(2)
    call cpu_time(cpu_t_start)

    AdvanceTime_fw: do j = 0, Nt, 1

      if (mod(j,sampling)==0) then
        time = t0_a+real(j,rp)*ht_a
        do i = 0, Nx+1, 1
          write(file_heat_fw,fmt_hmap) adimTime(-1, time), adimX(-1, x(i)), rescaleTemp(-1, Tij(i))
        end do
        write(file_heat_fw,*) ""
      end if

      AdvanceSpace_fw: do i = 1, Nx, 1
        call HeatExplicitForw(Tij(i-1), Tij(i), Tij(i+1), eta, Tij_next(i))
      end do AdvanceSpace_fw

      Tij(:) = Tij_next(:)

    end do AdvanceTime_fw

    call EmptyBar(2)

    call cpu_time(cpu_t_stop)
    print* , "Tiempo de CPU:", abs(cpu_t_stop-cpu_t_start), "seg"
    print*, "--------------------------------------------------------"
  ! ---------------------------------------------------------------------------


  ! Backward ------------------------------------------------------------------
    print*, "Calculo con metodo implicito (backward)"

    ! Creo arreglo de datos de temperatura al tiempo ti
    call InitBar(1)

    ! Creo arreglos para diagonales
    allocate(dinf(2:Nx), dcent(1:Nx), dsup(1:Nx-1), B(1:Nx))
    alpha     = 1._rp + 2._rp*eta
    dinf(:)   = -eta        !> Diagonal inferior
    dsup(:)   = -eta        !> Diagonal superior
    dcent(:)  = alpha       !> Diagonal principal

    call cpu_time(cpu_t_start)

    do j = 0, Nt, 1

      B(:) = Tij(1:Nx)

      if (mod(j,sampling)==0) then
        time = t0_a+real(j,rp)*ht_a
        do i = 0, Nx+1, 1
          write(file_heat_bw,fmt_hmap) adimTime(-1, time), adimX(-1, x(i)), rescaleTemp(-1, Tij(i))
        end do
        write(file_heat_bw,*) ""
      end if

      do i = 1, Nx, 1
        call tridiag(dinf(:), dcent(:), dsup(:), B(:), Tij(1:Nx))
      end do

    end do

    deallocate(dinf, dcent, dsup, B)
    call EmptyBar(1)

    call cpu_time(cpu_t_stop)
    print*, "Tiempo de CPU:", abs(cpu_t_stop-cpu_t_start), "seg"
    print*, "--------------------------------------------------------"
  ! ---------------------------------------------------------------------------


  ! Crank-Nicolson ------------------------------------------------------------
    print*, "Calculo con metodo de Crank-Nicolson"

    ! Creo arreglo de datos de temperatura al tiempo ti
    call InitBar(1)

    ! Creo arreglos para diagonales
    allocate(dinf(2:Nx), dcent(1:Nx), dsup(1:Nx-1), B(1:Nx))
    alpha     = 2._rp/eta
    dinf(:)   = -1._rp      !> Diagonal inferior
    dsup(:)   = -1._rp      !> Diagonal superior
    dcent(:)  = alpha+2._rp !> Diagonal principal

    call cpu_time(cpu_t_start)

    do j = 0, Nt, 1

      if (mod(j,sampling)==0) then
        time = t0_a+real(j,rp)*ht_a
        do i = 0, Nx+1, 1
          write(file_heat_cn,fmt_hmap) adimTime(-1, time), adimX(-1, x(i)), rescaleTemp(-1, Tij(i))
        end do
        write(file_heat_cn,*) ""
      end if

      do i = 1, Nx, 1
        B(i) = Tij(i-1)+(alpha-2._rp)*Tij(i)+Tij(i+1)
      end do

      do i = 1, Nx, 1
        call tridiag(dinf(:), dcent(:), dsup(:), B(:), Tij(1:Nx))
      end do

    end do

    deallocate(dinf, dcent, dsup, B)
    call EmptyBar(1)

    call cpu_time(cpu_t_stop)
    print*, "Tiempo de CPU:", abs(cpu_t_stop-cpu_t_start), "seg"
    print*, "--------------------------------------------------------"
  ! ---------------------------------------------------------------------------
  deallocate(x)



  ! Errores -------------------------------------------------------------------

  ! ---------------------------------------------------------------------------
  ! Defino condiciones iniciales y de borde
  t_0 = 0._rp
  x_0 = 0._rp; x_f = 1._rp
  ! Temperatuta en Kelvin
  T_0x  = 373.15_rp
  T_t0  = 273.15_rp;  T_tl  = 273.15_rp

  t_f = 3000._rp  !> Tiempo de corrida
  Nx  = 100       !> ptos para evaluacion espacial
  ht  = 0.3_rp    !> Ancho de paso temporal
  ! Nx  = int(( x_f-x_0 )/sqrt( ht/1000._rp ))

  sampling = 300  !> Cada cuantos punos escribo en disco
  ! ---------------------------------------------------------------------------
  call InitConditions()
  ! ht_a  = 0.05001_rp*(hx_a*hx_a)

  ! -------------------------------------------------------
  print*, "Calculo de errores"
  call cpu_time(cpu_t_start)

  ! Creo arreglo de datos de temperatura al tiempo ti
  call InitBar(5)

  ! Creo arreglos para diagonales - Forward
  allocate(dinf(2:Nx), dcent(1:Nx), dsup(1:Nx-1), B(1:Nx))
  alpha     = 1._rp + 2._rp*eta
  dinf(:)   = -eta        !> Diagonal inferior
  dsup(:)   = -eta        !> Diagonal superior
  dcent(:)  = alpha       !> Diagonal principal

  ! Creo arreglos para diagonales - Crank-Nicolson
  allocate(dinf_cn(2:Nx), dcent_cn(1:Nx), dsup_cn(1:Nx-1), B_cn(1:Nx))
  alpha_cn    = 2._rp/eta
  dinf_cn(:)  = -1._rp          !> Diagonal inferior
  dsup_cn(:)  = -1._rp          !> Diagonal superior
  dcent_cn(:) = alpha_cn+2._rp  !> Diagonal principal


  AdvanceTime_err: do i = 0, Nt, 1

    if (mod(i,sampling)==0) then
      time = t0_a+real(i,rp)*ht_a

      do j = 1, Nx, 1
        call HeatAnalitic(T_0x, 3000, time, x(j), Tij_an(j))
      end do

      do j = 0, Nx+1, 1
        write(file_heat_er,fmt_err) adimTime(-1, time),               &
                                    adimX(-1, x(j)),                  &
                                    rescaleTemp(-1, Tij_fw(j)),       &
                                    tempError(Tij_an(j), Tij_fw(j)),  &
                                    rescaleTemp(-1, Tij_bw(j)),       &
                                    tempError(Tij_an(j), Tij_bw(j)),  &
                                    rescaleTemp(-1, Tij_cn(j)),       &
                                    tempError(Tij_an(j), Tij_cn(j))
      end do
      write(file_heat_er,*) ""
    end if

    B(:) = Tij_bw(1:Nx)
    do j = 1, Nx, 1
      B_cn(j) = Tij_cn(j-1)+(alpha_cn-2._rp)*Tij_cn(j)+Tij_cn(j+1)
    end do

    AdvanceSpace_err: do j = 1, Nx, 1
      call HeatExplicitForw(Tij_fw(j-1), Tij_fw(j), Tij_fw(j+1), eta, Tij_next(j))
      call tridiag(dinf(:), dcent(:), dsup(:), B(:), Tij_bw(1:Nx))
      call tridiag(dinf_cn(:), dcent_cn(:), dsup_cn(:), B_cn(:), Tij_cn(1:Nx))
    end do AdvanceSpace_err

    Tij_fw(:) = Tij_next(:)

  end do AdvanceTime_err

  deallocate(dinf, dcent, dsup, B)
  deallocate(dinf_cn, dcent_cn, dsup_cn, B_cn)
  call EmptyBar(5)

  call cpu_time(cpu_t_stop)
  print* , "Tiempo de CPU:", abs(cpu_t_stop-cpu_t_start), "seg"
  print*, "--------------------------------------------------------"
  ! ---------------------------------------------------------------------------
  deallocate(x)
  call CloseFiles()

  ! goto 10000
  ! Apartado h ----------------------------------------------------------------

  ! ---------------------------------------------------------------------------
  ! Defino condiciones iniciales y de borde
  t_0 = 0._rp
  x_0 = 0._rp; x_f = 1._rp
  ! Temperatuta en Kelvin
  T_0x  = 373.15_rp
  T_t0  = 273.15_rp;  T_tl  = 273.15_rp

  t_f = 3000._rp  !> Tiempo de corrida
  Nx  = 100       !> ptos para evaluacion espacial
  ht  = 0.03_rp    !> Ancho de paso temporal

  sampling = 300  !> Cada cuantos punos escribo en disco
  ! ---------------------------------------------------------------------------
  call InitConditions()

  ! -------------------------------------------------------
  print*, "Calculo de errores"
  call cpu_time(cpu_t_start)

  ! Creo arreglo de datos de temperatura al tiempo ti
  call InitBar(5)

  ! Creo arreglos para diagonales - Forward
  allocate(dinf(2:Nx), dcent(1:Nx), dsup(1:Nx-1), B(1:Nx))
  alpha     = 1._rp + 2._rp*eta
  dinf(:)   = -eta        !> Diagonal inferior
  dsup(:)   = -eta        !> Diagonal superior
  dcent(:)  = alpha       !> Diagonal principal

  ! Creo arreglos para diagonales - Crank-Nicolson
  allocate(dinf_cn(2:Nx), dcent_cn(1:Nx), dsup_cn(1:Nx-1), B_cn(1:Nx))
  alpha_cn    = 2._rp/eta
  dinf_cn(:)  = -1._rp          !> Diagonal inferior
  dsup_cn(:)  = -1._rp          !> Diagonal superior
  dcent_cn(:) = alpha_cn+2._rp  !> Diagonal principal

  er_fw_done = .FALSE.
  er_bw_done = .FALSE.
  er_cn_done = .FALSE.

  do i = 0, Nt, 1

    time = t0_a+real(i,rp)*ht_a
    do j = 1, Nx, 1
      call HeatAnalitic(T_0x, 3000, time, x(j), Tij_an(j))
    end do

    B(:) = Tij_bw(1:Nx)
    do j = 1, Nx, 1
      B_cn(j) = Tij_cn(j-1)+(alpha_cn-2._rp)*Tij_cn(j)+Tij_cn(j+1)
    end do

   do j = 1, Nx, 1
      call HeatExplicitForw(Tij_fw(j-1), Tij_fw(j), Tij_fw(j+1), eta, Tij_next(j))
      call tridiag(dinf(:), dcent(:), dsup(:), B(:), Tij_bw(1:Nx))
      call tridiag(dinf_cn(:), dcent_cn(:), dsup_cn(:), B_cn(:), Tij_cn(1:Nx))
    end do

    Tij_fw(:) = Tij_next(:)

    er_fw = tempError(Tij_an(j), Tij_fw(j))
    er_bw = tempError(Tij_an(j), Tij_bw(j))
    er_cn = tempError(Tij_an(j), Tij_cn(j))

    if ( (er_fw_done .eqv. .FALSE.) .and. ( er_fw < er_check ) ) then
      er_fw_done = .TRUE.
      call cpu_time(cpu_t_stop)
      print* , "Tiempo de CPU para Forward:", abs(cpu_t_stop-cpu_t_start), "seg"
    else if ( (er_bw_done .eqv. .FALSE.) .and. ( er_bw < er_check )  ) then
      er_bw_done = .TRUE.
      call cpu_time(cpu_t_stop)
      print* , "Tiempo de CPU para Backward:", abs(cpu_t_stop-cpu_t_start), "seg"
    else if ( (er_cn_done .eqv. .FALSE.) .and. ( er_cn < er_check ) ) then
      er_cn_done = .TRUE.
      call cpu_time(cpu_t_stop)
      print* , "Tiempo de CPU para C-N:", abs(cpu_t_stop-cpu_t_start), "seg"
    endif

  end do

  if ( (er_fw_done .eqv. .FALSE.) .OR. (er_bw_done .eqv. .FALSE.) .OR. (er_cn_done .eqv. .FALSE.) ) then
    print* , "No se alcanzó el error mínimo para alguno de los métodos"
  endif

  deallocate(dinf, dcent, dsup, B)
  deallocate(dinf_cn, dcent_cn, dsup_cn, B_cn)
  call EmptyBar(5)
  deallocate(x)
  print*, "--------------------------------------------------------"
  ! ---------------------------------------------------------------------------
  ! 10000 continue

end program lab02_p1
