module p1_GlobalVariables

  use precision_interface

  implicit none
  real(rp)    ::  K=237._rp, C=900._rp, rho=2700._rp
  integer(ip) ::  Nt, Nx, sampling
  real(rp)    ::  t_0, t_f, x_0, x_f, T_0x, T_t0, T_tl, ht, hx, fax, fat,      &
                  t0_a, tf_a, x0_a, xf_a, ht_a, hx_a, time, eta,               &
                  T_0x_D, T_t0_D, T_tl_D
  real(rp),allocatable  ::  x(:), Tij(:), Tij_prev(:), Tij_an(:), Tij_bw(:),   &
                            Tij_fw(:), Tij_next(:), Tij_cn(:)

  integer, parameter  ::  file_heat_an=100, file_heat_fw=101, file_heat_bw=102,&
                          file_heat_cn=103, file_heat_er=104

  character(len=30),parameter ::  fmt_hmap="(3(ES22.4, X))",                   &
                                  fmt_err="(8(ES22.4, X))"


contains

  subroutine OpenFiles()
    implicit none
    open(unit=file_heat_an, file="./output/heatmap_analitic.dat",         status="replace", action="write")
    open(unit=file_heat_fw, file="./output/heatmap_forward.dat",          status="replace", action="write")
    open(unit=file_heat_bw, file="./output/heatmap_backward.dat",         status="replace", action="write")
    open(unit=file_heat_cn, file="./output/heatmap_crank-nicolson.dat",   status="replace", action="write")
    open(unit=file_heat_er, file="./output/heatmap_methods_error.dat",    status="replace", action="write")
    return
  end subroutine OpenFiles

  subroutine CloseFiles()
    implicit none
    close(file_heat_an)
    close(file_heat_fw)
    close(file_heat_bw)
    close(file_heat_cn)
    close(file_heat_er)
    return
  end subroutine CloseFiles


  subroutine InitConditions()
    implicit none
    integer   ::  i

    ! Correccion para calculo (paso a Celsius)
    ! T_0x = K2C(T_0x)
    ! T_t0 = K2C(T_t0); T_tl = K2C(T_tl)
    T_0x_D = T_0x
    T_t0_D = T_t0; T_tl_D = T_tl
    T_0x = rescaleTemp(1, T_0x)
    T_t0 = rescaleTemp(1, T_t0); T_tl = rescaleTemp(1, T_tl)

    ! Adimensionalizo
    fax = 1._rp/(x_f-x_0)
    fat = (K/(C*rho))*fax**2  !> Factor de adim para t

    x0_a  = adimX(1, x_0) ; xf_a = adimX(1, x_f)
    t0_a  = adimTime(1, t_0)  ; tf_a = adimTime(1, t_f)
    ht_a  = ht*fat
    hx_a  = ( xf_a-x0_a )/real(Nx, rp)      !> Ancho de paso espacial


    Nt    = int(ceiling((tf_a-t0_a)/ht_a))  !> Ptos para evolucion temporal

    ! Parametros para iteracion
    eta = ht_a/(hx_a*hx_a)
    ! Chequeo estabilidad
    if (eta > 0.5_rp) &
      stop "NO satisface criterio de establilidad para Forward"

    ! Arreglo puntos de la barra
    allocate( x(0:Nx+1) )
    do i = 0, Nx+1, 1
      x(i) = x0_a+real(i,rp)*hx_a
    end do

    return
  end subroutine InitConditions


  subroutine InitBar(cant)
    implicit none
    integer   ::  cant
    ! Tx > temperatura a lo largo de la barra
    select case (cant)
    case (1)
      allocate( Tij(0:Nx+1) )
      Tij(0)    = T_t0
      Tij(Nx+1) = T_t0
      Tij(1:Nx) = T_0x
    case (2)
      allocate( Tij(0:Nx+1), Tij_next(0:Nx+1) )
      Tij(0)    = T_t0
      Tij(Nx+1) = T_t0
      Tij(1:Nx) = T_0x
      Tij_next = Tij
    case (5)
      allocate( Tij_an(0:Nx+1), Tij_bw(0:Nx+1), Tij_fw(0:Nx+1), Tij_next(0:Nx+1), Tij_cn(0:Nx+1) )
      Tij_an(0)    = T_t0
      Tij_an(Nx+1) = T_t0
      Tij_an(1:Nx) = T_0x
      Tij_next = Tij_an
      Tij_bw   = Tij_an
      Tij_fw   = Tij_an
      Tij_cn   = Tij_an
    end select
    return
  end subroutine InitBar

  subroutine EmptyBar(cant)
    implicit none
    integer   ::  cant
    select case (cant)
    case (1)
      deallocate( Tij )
    case (2)
      deallocate( Tij, Tij_next )
    case (5)
      deallocate( Tij_an, Tij_bw, Tij_fw, Tij_next, Tij_cn )
    end select
  end subroutine EmptyBar


  function adimX(direction, xx)
    !direction = 1  > adimensionaliza x
    !direction = -1 > revierte adimensionalizacion
    implicit none
    integer, intent(in)   ::  direction
    real(rp), intent(in)  ::  xx
    real(rp)              ::  adimX
    select case (direction)
    case (1)
      adimX = (xx-x_0)*fax
      return
    case (-1)
      adimX = xx/fax + x_0
      return
    end select
  end function adimX

  function adimTime(direction, tt)
    !direction = 1  > adimensionaliza tiempo
    !direction = -1 > revierte adimensionalizacion
    implicit none
    integer, intent(in)   ::  direction
    real(rp), intent(in)  ::  tt
    real(rp)              ::  adimTime
    select case (direction)
    case (1)
      adimTime = fat*tt
      return
    case (-1)
      adimTime = tt/fat
      return
    end select
  end function adimTime


  function rescaleTemp(direction, tt)
    ! Lleva temperatura inicial a 100 y temperatura en los extremos a 0
    ! para cualquier par de temperaturas dadas. Esto para ser compatible
    ! con las soluciones teóricas sin necesidad de modificarlas y poder
    ! solucionar cualquier par de condiciones iniciales y de contorno;
    ! siempre y cuando las temperaturas de contorno sean iguales.
    !
    ! direction = 1  > reescala Temperatura a [0, 100]
    ! direction = -1 > revierte reescaleo
    !
    implicit none
    integer, intent(in)   ::  direction
    real(rp), intent(in)  ::  tt
    real(rp)              ::  rescaleTemp, fatemp, bcte
    fatemp  = 100._rp/(T_0x_D-T_t0_D)
    bcte    = -1._rp*fatemp*T_t0_D
    select case (direction)
    case (1)
      rescaleTemp = tt*fatemp + bcte
      return
    case (-1)
      rescaleTemp = (tt-bcte)/fatemp
      return
    end select
  end function rescaleTemp


  function K2C(tt)
  ! Función que cambia de kelvin a celsius
    implicit none
    real(rp), intent(in)  ::  tt
    real(rp)              ::  K2C
    K2C = tt - 273.15_rp
    return
  end function K2C

  function C2K(tt)
  ! Función que cambia de celsius a kelvin
    implicit none
    real(rp), intent(in)  ::  tt
    real(rp)              ::  C2K
    C2K = tt + 273.15_rp
    return
  end function C2K

end module p1_GlobalVariables
