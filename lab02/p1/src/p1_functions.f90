module p1_functions

  use precision_interface
  use p1_GlobalVariables

  private
  public HeatAnalitic, HeatExplicitForw, tempError

contains

  subroutine HeatAnalitic(Temp_0, nmax, tt, xx,  Temp)
  ! -------------------------------------------------------
  ! Parametros de entrada:
  !
  ! Parametros de salida:
  !
  ! -------------------------------------------------------
    use precision, only: rp=>dp, ip
    use constantes, only: pi
    implicit none
    real(rp), intent(in)    :: Temp_0, tt, xx
    integer(ip), intent(in) :: nmax
    real(rp)                :: Temp, kn
    integer(ip)             :: n
      Temp = 0._rp
      do n = 1, nmax, 2
        kn = real(n,rp)*pi/(xf_a-x0_a)
        Temp = Temp + sin(kn*xx)*exp(-kn*kn*tt)/real(n,rp)
      end do
      Temp = Temp * 4._rp*Temp_0/pi
      return
  end subroutine HeatAnalitic


  subroutine HeatExplicitForw(Tiprev, Ti, Tisig, eta_, Tjsig)
  ! -------------------------------------------------------
  ! Parametros de entrada:
  !   Tiprev  : T i-1,j
  !   Ti      : T i,j
  !   Tisig   : T i+1,j
  !   eta     : paso_t/(paso_x)**2
  ! Parametros de salida:
  !   Tjsig   : T i,j+1
  ! -------------------------------------------------------
    use precision, only: rp=>dp, ip
    implicit none
    real(rp), intent(in)    :: Tiprev, Ti, Tisig, eta_
    real(rp), intent(inout) :: Tjsig
      Tjsig = eta_*Tiprev+(1._rp-2._rp*eta_)*Ti+eta_*Tisig
      return
  end subroutine HeatExplicitForw


  function tempError(xx, yy)
    implicit none
    real(rp), intent(in)    :: xx, yy
    real(rp)                :: tempError, tx, ty
      tx  = rescaleTemp(-1, xx)
      ty  = rescaleTemp(-1, yy)
      tempError = abs( (tx-ty) )
      return
  end function tempError

end module
