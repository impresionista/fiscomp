# line styles for Virdis palette

# line styles
set style line  1 lt 1 lc rgb '#440154' # dark purple
set style line  2 lt 1 lc rgb '#472c7a' # purple
set style line  3 lt 1 lc rgb '#3b518b' # blue
set style line  4 lt 1 lc rgb '#2c718e' # blue
set style line  5 lt 1 lc rgb '#21908d' # blue-green
set style line  6 lt 1 lc rgb '#27ad81' # green
set style line  7 lt 1 lc rgb '#5cc863' # green
set style line  8 lt 1 lc rgb '#aadc32' # lime green
set style line  9 lt 1 lc rgb '#fde725' # yellow

# palette
set palette defined ( 0 "#440154",\
    	    	          1 "#472c7a",\
		                  2 "#3b518b",\
		                  3 "#2c718e",\
                      4 "#21908d",\
                      5 "#27ad81",\
		                  6 "#5cc863",\
		                  7 "#aadc32",\
                      8 "#fde725" )
