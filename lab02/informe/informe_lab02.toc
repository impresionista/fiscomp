\babel@toc {spanish}{}\relax 
\contentsline {chapter}{\numberline {1}Compilación y ejecución}{1}{chapter.1}%
\contentsline {chapter}{\numberline {2}Problema 1: Ecuación de calor}{2}{chapter.2}%
\contentsline {section}{\numberline {2.1}Descripción teórica}{2}{section.2.1}%
\contentsline {section}{\numberline {2.2}Métodos de solución}{2}{section.2.2}%
\contentsline {subsection}{\numberline {2.2.1}Solución analítica}{3}{subsection.2.2.1}%
\contentsline {subsection}{\numberline {2.2.2}Métodos por elementos finítos}{3}{subsection.2.2.2}%
\contentsline {subsubsection}{Método de Euler hacia adelante (Forward)}{3}{section*.2}%
\contentsline {subsubsection}{Método de Euler hacia atrás (Backward)}{3}{section*.3}%
\contentsline {subsubsection}{Método de Crank-Nicolson}{4}{section*.4}%
\contentsline {section}{\numberline {2.3}Simulación}{4}{section.2.3}%
\contentsline {subsection}{\numberline {2.3.1}Características de la simulación numérica}{4}{subsection.2.3.1}%
\contentsline {subsection}{\numberline {2.3.2}Apartados a, b, c, d, f y g}{4}{subsection.2.3.2}%
