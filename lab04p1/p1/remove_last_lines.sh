#!/bin/bash

declare -a files=(
                "./output/10_rand_200.out"
                "./output/20_rand_200.out"
                "./output/40_rand_200.out"
                "./output/40_rand_226.out"
                "./output/40_rand_330.out"
                "./output/40_up_200.out"
                "./output/40_up_226.out"
                "./output/40_up_330.out"
                "./output/128_rand_200.out"
                "./output/128_rand_222.out"
                "./output/128_rand_226.out"
                "./output/128_rand_250.out"
                "./output/128_rand_330.out"
                "./output/128_up_200.out"
                "./output/128_up_226.out"
                "./output/128_up_330.out"
                )

for file in "${files[@]}"; do
  head -n -2 "$file" > output.tmp && mv output.tmp "$file"
done
