module precision_interface

! -----------------------------------------------------------------------------
! Modulo que se utiliza para seleccionar de forma uniforme la precision para
! programas y librerías
! -----------------------------------------------------------------------------

  use precision, only: rp=>dp, qp=>qp, ip

end module precision_interface
