module p1Mod

  use precision_interface
  use mzranmod,   only: rmzran

  implicit none

contains

! --------------------------------    -------------------------------
! SUBRUTINAS
! --------------------------------    -------------------------------

  ! -----------------  INICIALIZA ARREDLO DE SPINS  -----------------
  subroutine initState(array, initType, logfile)
    !Inicializa arreglo de particulas
    ! Variables:
    !   array:    arreglo a inicializar
    !   initType: tipo de inicialización
    !   logfile:  arghivo de log de ejecución
    !             si no se define va a standard I/O

    implicit none
    character(12), intent(in) ::  initType
    integer(2), intent(inout) ::  array(:,:)
    integer, optional         ::  logfile
    integer       ::  i, j, N
    character(10) ::  tipo

      N = ubound(array, 1)

      tipo = trim(initType)
      select case (tipo)
        case("rand")
          do i = 1, N
            do j = 1, N
              array(i,j) = rndSpinState()
            end do
          end do
          if (present(logfile)) then
            write(logfile,*) 'Arreglo inicial de spins:'
            call writeArray(array, logfile)
          end if

        case ("up")
          array(:,:) = +1
          if (present(logfile)) then
            write(logfile,*) 'Arreglo inicial de spins:'
            call writeArray(array, logfile)
          end if

        case ("down")
          array(:,:) = -1
          if (present(logfile)) then
            write(logfile,*) 'Arreglo inicial de spins:'
            call writeArray(array, logfile)
          end if

        case default
          if ( present(logfile) ) then
            write(logfile,*)  "ERROR: estado inicial mal elegido."
            write(logfile,*)  tipo, " no es una opción válida. => STOP"
          else
            write(*,*)  "ERROR: estado inicial mal elegido."
            write(*,*)  tipo, " no es una opción válida. => STOP"
          end if

          stop

      end select

    return
  end subroutine initState
  ! -------------------------------    ------------------------------

  ! -----------------------  ACTUALIZA BORDES  ----------------------
  subroutine updateBorders()
    ! Actualiza los bordes del part_array

    use p1Variables

    implicit none

      part_array( 1:Nsites , 0        )  = part_array( 1:Nsites , Nsites   )
      part_array( 1:Nsites , Nsites+1 )  = part_array( 1:Nsites , 1        )
      part_array( 0        , 1:Nsites )  = part_array( Nsites   , 1:Nsites )
      part_array( Nsites+1 , 1:Nsites )  = part_array( 1        , 1:Nsites )

    return
  end subroutine updateBorders
  ! -------------------------------    ------------------------------

  ! ----------------------  EVOLUCIONA SISTEMA  ---------------------
  subroutine mcStep(flag)

    use p1Variables

    implicit none
    integer, intent(in) ::  flag
    integer             ::  ind=0, i=0, j=0
    real(rp)            ::  Nrand=0._rp
    real(qp)            ::  stepE=0._rp, stepM=0._rp

      if (flag==0) then
        do i = 1, Nsites, 1
          do j = 1, Nsites, 1

            localE =  part_array( i   , j-1 ) + &
                      part_array( i   , j+1 ) + &
                      part_array( i-1 , j   ) + &
                      part_array( i+1 , j   )

            ind = (localE*part_array(i,j)+4)/2+1

            if ( ind<3 ) then
              part_array(i,j) = -part_array(i,j)
            else
              Nrand = rmzran()
              if ( FNBoltz(ind) >= Nrand ) then
                part_array(i,j) = -part_array(i,j)
              end if
            end if

            ! call updateBorders()
            part_array(i,0) = part_array(i,Nsites)
            part_array(i,Nsites+1) = part_array(i,1)
            part_array(0,j) = part_array(Nsites, j)
            part_array(Nsites+1,j) = part_array(1, j)
          end do
        end do

      else if (flag==1) then

        do i = 1, Nsites, 1
          do j = 1, Nsites, 1

            ! Calculo energia local
            localE =  part_array( i   , j-1 ) + &
                      part_array( i   , j+1 ) + &
                      part_array( i-1 , j   ) + &
                      part_array( i+1 , j   )

            ind = (localE*part_array(i,j)+4)/2+1

            stepE = 0._rp
            stepM = 0._rp

            if ( ind<3 ) then
              part_array(i,j) = -part_array(i,j)
              stepE = FNEnergy(ind)
              stepM = 2._rp*real(part_array(i,j),rp)
            else
              Nrand = rmzran()
              if ( FNBoltz(ind) >= Nrand ) then
                part_array(i,j) = -part_array(i,j)
                stepE = FNEnergy(ind)
                stepM = 2._rp*real(part_array(i,j),rp)
              end if
            end if

            ! call updateBorders()
            part_array(i,0) = part_array(i,Nsites)
            part_array(i,Nsites+1) = part_array(i,1)
            part_array(0,j) = part_array(Nsites, j)
            part_array(Nsites+1,j) = part_array(1, j)

          end do
        end do

        totalE  = systemEnergy(1)
        totalM  = systemMagnet(1)

      end if

    return
  end subroutine mcStep
  ! -------------------------------    ------------------------------

  ! ---------------------  CALCULA CORRELACION  ---------------------
  subroutine correlation()

    use p1Variables

    implicit none
    integer(8), save    ::  counter=0
    integer(ip)         ::  last, next, i

      if ( counter >= Nsample ) then
        counter=0
        localMagCorr = 0._rp
        localEneCorr = 0._rp
      end if

      counter = counter+1

      last  = mod(counter-1, Ncorr) + 1
      localMagCorr(last) = totalM
      localEneCorr(last) = totalE

      if ( counter < Ncorr ) return

      ! Normalizacion
      normM2  = normM2 + localMagCorr(last) * localMagCorr(last)
      normE2  = normE2 + localEneCorr(last) * localEneCorr(last)

      do i = 1, Ncorr
        next  = mod(counter-i, Ncorr) + 1
        corrMagN(i-1) = corrMagN(i-1) + localMagCorr(last) * localMagCorr(next)
        corrEneN(i-1) = corrEneN(i-1) + localEneCorr(last) * localEneCorr(next)
      end do

    return
  end subroutine correlation
  ! -------------------------------    ------------------------------

  ! ------------------  ESCRIBE ARREGLO A ARCHIVO  ------------------
  subroutine writeArray(part_array, of)

    implicit none
    integer(2), intent(in)        ::  part_array(:,:)
    integer, intent(in), optional ::  of
    integer ::  i

      if ( present(of) ) then
        do i = lbound(part_array,1), ubound(part_array, 1)
          write(of,*) part_array(i,:)
        end do
        write(of,*) ''
        write(of,*) ''
      else
        do i = lbound(part_array,1), ubound(part_array, 1)
          write(*,*) part_array(i,:)
        end do
        write(*,*) ''
        write(*,*) ''
      end if

    return
  end subroutine writeArray
  ! -------------------------------    ------------------------------

  ! -----------  ESCRIBE ARREGLO A ARCHIVO Y LO REBOBINA  -----------
  subroutine writePartialAdvance(part_array, of)

    implicit none
    integer(2), intent(in)        ::  part_array(:,:)
    integer, intent(in), optional ::  of

      call writeArray(part_array, of)
      rewind(of)

    return
  end subroutine writePartialAdvance
  ! -------------------------------    ------------------------------

  ! -----------  ESCRIBE ARREGLO A ARCHIVO Y LO REBOBINA  -----------
  subroutine  writeCorrVector(vector, of)

    implicit none
    real(rp), intent(in)              :: vector(:,:)
    integer(ip), intent(in), optional :: of
    integer ::  i

      if ( present(of) ) then
        do i = lbound(vector,1), ubound(vector, 1)
          write(of,*) vector(i,:)
        end do
      else
        do i = lbound(vector,1), ubound(vector, 1)
          write(*,*) vector(i,:)
        end do
        write(*,*) ''
        write(*,*) ''
      end if

    return
  end subroutine writeCorrVector
  ! -------------------------------    ------------------------------

  ! ----------------------  ELIGE COORD RANDOM  ---------------------
  subroutine pickRndCoord(place)
    use p1Variables
    implicit none
    integer(ip),intent(out) :: place(2)

      place = (/ int(real(Nsites,rp)*rmzran())+1, int(real(Nsites,rp)*rmzran())+1 /)

    return
  end subroutine pickRndCoord
  ! -------------------------------    ------------------------------

! --------------------------------    -------------------------------
! FUNCIONES
! --------------------------------    -------------------------------

  ! -------------------------  SPIN RANDOM  -------------------------
  function rndSpinState()
    implicit none
    integer(2)  ::  rndSpinState
    real(rp)    ::  dec
      dec = rmzran()
      if ( dec<0.5_rp ) then
        rndSpinState  = -1
      else
        rndSpinState  = 1
      end if
    return
  end function rndSpinState
  ! -------------------------------    ------------------------------

  ! ---------------------------  ENERGIA  ---------------------------
  function systemEnergy(nn)
    use p1Variables
    implicit none
    integer, intent(in) :: nn
    real(qp)  ::  systemEnergy
    integer   ::  i, j

      systemEnergy  =  0._qp
      do i =  1, Nsites
        do j = 1, Nsites
          systemEnergy =  systemEnergy +                                      &
                            real( part_array(i,j) *                           &
                                  (part_array(i,j+1) + part_array(i+1,j)),qp  &
                                )
        end do
      end do

      systemEnergy  = (-systemEnergy)**nn

    return
  end function systemEnergy
  ! -------------------------------    ------------------------------

  ! ------------------------  MAGNETIZACION  ------------------------
  function systemMagnet(nn)
    use p1Variables
    use constantes, only: K_boltzman, bohr_mag
    implicit none
    integer, intent(in) :: nn
    real(qp)  ::  systemMagnet
    integer   ::  i

      systemMagnet  =  0._qp
      do i =  1, Nsites
          systemMagnet = systemMagnet + &
                          real(sum((part_array(i,1:Nsites)),dim=1),qp)
      end do

      systemMagnet  = systemMagnet**nn

    return
  end function systemMagnet
  ! -------------------------------    ------------------------------

  ! ------------------------  PASO TEMPORAL  ------------------------
  function deltaTemp(x)
    use precision, only: rp=>dp
    use p1Variables

    implicit none
    real(rp)  ::  deltaTemp
    real(rp), intent(in)  ::  x
    real(rp)  ::  a, b, c, dTemp_max, cte1, cte2, cte3!, dMin

      dTemp_max = dTemp_min*Nsample
      ! dMin  = (Temp_f_norm-Temp_i_norm)*dTemp_min
      ! dTemp_max = (Temp_f_norm-Temp_i_norm)/(dTemp_min*Nsample)

      cte1  = (Temp_f_norm-Temp_c_norm)*Temp_i_norm - &
              Temp_c_norm*Temp_f_norm + Temp_c_norm**2

      cte2  = Temp_i_norm+Temp_f_norm

      cte3  = -Temp_c_norm*cte2+Temp_c_norm**2

      cte3  = Temp_f_norm*Temp_i_norm*dTemp_min + &
                (-Temp_c_norm*cte2 + Temp_c_norm**2)  * dTemp_max

      a = (dTemp_min-dTemp_max)/cte1
      b = -cte2*(dTemp_min-dTemp_max)/cte1
      c = cte3/cte1

      deltaTemp = (a*x**2+b*x+c)

    return
  end function deltaTemp
  ! -------------------------------    ------------------------------

  ! -------------------------  TEMPERATURA --------------------------
  function dTemp(x)

    use p1Variables
    implicit none
    real(rp)  ::  dTemp
    real(rp), intent(in)  ::  x
    real(rp)  ::  temp_interval, check_min, check_max

      temp_interval = Temp_f_norm-Temp_i_norm

      check_min = Temp_c_norm - 0.05_rp*temp_interval
      check_max = Temp_c_norm + 0.05_rp*temp_interval

      if ( (check_min<x) .and. (x<=check_max) ) then
        dTemp = 0.25_rp*temp_interval/real(Nsample,rp)
      else
        dTemp = 1.5_rp*temp_interval/real(Nsample,rp)
      end if

    return
  end function dTemp
  ! -------------------------------    ------------------------------

end module p1Mod
