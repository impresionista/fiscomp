module p1Variables

  use precision_interface
  use constantes, only: K_boltzman, bohr_mag

  implicit none

  character(len=80)   ::  init_state_type
  integer             ::  Nsites, Nsteps, Nsample, Ndisc, localE, Ntemp,      &
                          Nmed, Ncorr, Nexpe
  real(rp)            ::  J_coup, T_sys, T_norm, FNEnergy(5), FNBoltz(5),     &
                          deltaE, deltaM,  temp_init, Temp_i, Temp_f,         &
                          Temp_i_norm, Temp_f_norm, Temp_c_norm, Temp_step,   &
                          Temp_crit, dTemp_min, normM2, normE2
  real(qp)            ::  totalE, totalM, localM, meanE, meanM, totalE2,      &
                          meanE2, meanE_sh, totalM2, meanM2, totalM4, meanM4, &
                          specHeat, magSuscept, binderC, realSteps
  real(rp), allocatable ::  corrEneN(:), corrMagN(:),                         &
                            localMagCorr(:), localEneCorr(:)

  integer, parameter  ::  logUnit=100, outputUnit=101, partialUnit=102,       &
                          socketUnitTcte=110, inputUnit=111, physicsUnit=112, &
                          correUnit=113, socketUnitTswep=120, swepUnit=121,   &
                          socketUnitMag=130, magCard=131
  integer(2), allocatable :: part_array(:,:), part_mask(:,:)

contains

  ! ---------------------  INICIALIZA ARCHIVOS  ---------------------
  subroutine initFiles()
    implicit none

    ! --------------------------  ARCHIVOS  -------------------------
    ! Archivos de log
      open(unit=logUnit,  file='./p1.log',  position='append')
    ! Socket de entrada
      open(unit=socketUnitTcte,   file='./evol_Tcte.socket',  action='read',  &
            status='old')
      open(unit=socketUnitTswep,  file='./evol_Tswep.socket', action='read',  &
            status='old')
      open(unit=socketUnitMag,    file='./evol_mag.socket',   action='read',  &
            status='old')
      call initSockets()
    ! Archivo de avance parcial
      open(unit=partialUnit,      file='./output/av_parcial.in',              &
            action='readwrite')
    ! ------------------------------    -----------------------------

    return
  end subroutine initFiles
  ! -------------------------------    ------------------------------

  ! ----------------  INICIALIZA SOCKETS DE LECTURA  ----------------
  subroutine initSockets()
    implicit none
    integer :: i

      rewind(socketUnitTcte)
      do i = 1, 4
        read(socketUnitTcte,*)
      end do

      rewind(socketUnitTswep)
      do i = 1, 4
        read(socketUnitTswep,*)
      end do

      rewind(socketUnitMag)
      do i = 1, 4
        read(socketUnitMag,*)
      end do

    return
  end subroutine initSockets
  ! -------------------------------    ------------------------------

  ! --------------  LEE SOCKET PARA EVOLUCION TEMPORAL  -------------
  subroutine readSocketTcte(ioState)
    implicit none
    character(len=1024) ::  input_file_name, input_file, output_file1,  &
                            output_file2, output_file3
    integer, optional   ::  ioState
    integer ::  io

      read(socketUnitTcte, *, iostat=io)  input_file_name
      if ( present(ioState) ) then
        ioState = io
        if (ioState<0) return
      end if
      input_file  = './input/'//trim(input_file_name)//'.in'

      ! Archivo de entrada
      open(unit=inputUnit,  file=input_file, action='read', status='old')
      write(logUnit,*) 'Tarjeta de entrada: ', input_file

      ! Archivos de salida
      output_file1  = './output/'//trim(input_file_name)//'.out'
      open(unit=outputUnit,   file=output_file1,  status='replace')
      output_file2  = './output/'//trim(input_file_name)//'_physics.out'
      open(unit=physicsUnit,  file=output_file2,  status='replace')
      output_file3  = './output/'//trim(input_file_name)//'_correlation.out'
      open(unit=correUnit,    file=output_file3,  status='replace')

      ! ---------------  CARGO PARAMETROS DESDE TARJETA  --------------
        read(inputUnit,*) Nsites          !01> Nro de particulas por lado del arrego
        read(inputUnit,*) Nsteps          !02> Nro de pasos por sitio
        read(inputUnit,*) Ncorr           !03> Nro de de MC para testear correlacion
        read(inputUnit,*) Nsample         !04> Intervalo de pasos para escribir muestra
        read(inputUnit,*) Ndisc           !05> Nro de pasos a descartar por termalizacion
        read(inputUnit,*) init_state_type !06> Tipo de cond inicial
        read(inputUnit,*) J_coup          !07> Intensidad de acoplamiento
        read(inputUnit,*) T_sys           !08> Temperatura de acoplamiento
      ! ------------------------------    -----------------------------

      Nexpe = Nsteps/Nsample

      call paramInit()
    return
  end subroutine readSocketTcte
  ! -------------------------------    ------------------------------

  ! ------------  LEE SOCKET PARA BARRIDO DE TEMPERATURA  -----------
  subroutine readSocketTswep(ioState)
    implicit none
    character(len=1024) ::  input_file_name, input_file, output_file
    integer, optional   ::  ioState
    integer ::  io

      read(socketUnitTswep,*, iostat=io) input_file_name
      if ( present(ioState) ) then
        ioState = io
        if (ioState<0) return
      end if
      input_file  = './input/'//trim(input_file_name)//'.in'

      ! Archivo de entrada
      open(unit=swepUnit,  file=input_file,  action='read',  status='old')
      write(logUnit,*) 'Tarjeta de entrada: ', input_file

      ! Archivos de salida
      output_file = './output/'//trim(input_file_name)//'_TempSwep.out'
      open(unit=outputUnit,  file=output_file, status='replace')

      ! ---------------  CARGO PARAMETROS DESDE TARJETA  --------------
        read(swepUnit,*) Nsites           !01>  Nro de particulas por lado del arrego
        read(swepUnit,*) Nsteps           !02>  Nro de pasos de MC por temperatura
        read(swepUnit,*) Nsample          !03>  Nro de temperaturas a medir
        read(swepUnit,*) Ndisc            !04>  Nro de pasos para termalizacion
        read(swepUnit,*) Nmed             !05>  Nro de pasos de MC a promediar
        read(swepUnit,*) init_state_type  !06>  Tipo de cond inicial
        read(swepUnit,*) J_coup           !07>  Intensidad de acoplamiento
        read(swepUnit,*) Temp_i           !08>  Temperatura inicial NORMALIZADA
        read(swepUnit,*) Temp_f           !09>  Temperatura final NORMALIZADA
        read(swepUnit,*) Temp_crit        !10>  Temperatura critica NORMALIZADA
      ! ------------------------------    -----------------------------

      call paramSwepInit()

    return
  end subroutine readSocketTswep
  ! -------------------------------    ------------------------------

  ! ------------  LEE SOCKET PARA MAGNETIZACION  -----------
  subroutine readSocketMag(ioState)
    implicit none
    character(len=1024) ::  input_file_name, input_file, output_file
    integer, optional   ::  ioState
    integer ::  io

      read(socketUnitMag, *, iostat=io)  input_file_name
      if ( present(ioState) ) then
        ioState = io
        if (ioState<0) return
      end if
      input_file  = './input/'//trim(input_file_name)//'.in'

      ! Archivo de entrada
      open(unit=magCard,  file=input_file, action='read', status='old')
      write(logUnit,*) 'Tarjeta de entrada: ', input_file

      ! Archivos de salida
      output_file = './output/'//trim(input_file_name)//'.out'
      open(unit=outputUnit,   file=output_file,   status='replace')

      ! ---------------  CARGO PARAMETROS DESDE TARJETA  --------------
        read(magCard,*) Nsites            !01>  Nro de particulas por lado del arrego
        read(magCard,*) init_state_type   !02>  Tipo de cond inicial
        read(magCard,*) Nsteps            !03>  Nro de pasos de MC en c/experimento
        read(magCard,*) Ndisc             !04>  Nro de iteraciones para termalizar
        read(magCard,*) Nexpe             !05>  Nro de experimetos
        read(magCard,*) Nsample           !06>  Cada cuantos pasos de MC tomo una muestra
        read(magCard,*) T_sys             !07>  Temperatura del sistema
      ! ------------------------------    -----------------------------

      call paramMagInit()

    return
  end subroutine readSocketMag
  ! -------------------------------    ------------------------------

  ! --------------------  INICIALIZA PARAMETROS  --------------------
  subroutine paramInit()

    implicit none
      ! Temperatura nomarlizada
      ! T_norm  = K_boltzman*T_sys/J_coup
      T_norm  = T_sys !>TEST??

      call energyInit()

      ! Arreglo inicial de spines
      allocate(part_array(0:Nsites+1,0:Nsites+1))
      part_array = 0
      ! Arreglo de numeros de autocorrelacion
      allocate(corrEneN(0:Ncorr-1), corrMagN(0:Ncorr-1))
      allocate(localMagCorr(0:Ncorr), localEneCorr(0:Ncorr))
      corrEneN  = 0._rp
      corrMagN  = 0._rp

    return
  end subroutine paramInit

  subroutine paramSwepInit()
    implicit none

      ! Temperatura nomarlizada
      ! Temp_i_norm  = K_boltzman*Temp_i/J_coup
      ! T_f_norm  = K_boltzman*T_f/J_coup
      Temp_i_norm = Temp_i !>TEST??
      Temp_f_norm = Temp_f !>TEST??
      Temp_c_norm = Temp_crit

      T_norm  = Temp_i_norm

      realSteps = real(Nsteps,rp)

      ! Arreglo inicial de spines
      allocate(part_array(0:Nsites+1,0:Nsites+1))
      part_array = 0
    return
  end subroutine paramSwepInit

  subroutine paramMagInit()
    implicit none
      ! Temperatura nomarlizada
      ! T_norm  = K_boltzman*T_sys/J_coup
      T_norm  = T_sys !>TEST??

      call energyInit()

      ! Arreglo inicial de spines
      allocate(part_array(0:Nsites+1,0:Nsites+1))
      part_array = 0

    return
  end subroutine paramMagInit

  subroutine energyInit()
    implicit none
      ! Energias posibles por primeros vecinos
      FNEnergy  = (/ -8._rp, -4._rp, 0._rp, &
                      4._rp, 8._rp /)
      ! Boltzmann posible por primeros vecinos
      FNBoltz = (/  exp(-FNEnergy(1)/T_norm), exp(-FNEnergy(2)/T_norm), &
                    exp(-FNEnergy(3)/T_norm), exp(-FNEnergy(4)/T_norm), &
                    exp(-FNEnergy(5)/T_norm) /)
    return
  end subroutine energyInit
  ! -------------------------------    ------------------------------

  ! ------------------  DEALOCA VARIABLES ALOCADAS  -----------------
  ! -----------------  Y CIERRA ARCHIVOS DE SALIDA  -----------------
  subroutine endRunTempCte()
    implicit none
      deallocate(part_array)
      deallocate(corrEneN)
      deallocate(corrMagN)
      deallocate(localMagCorr, localEneCorr)
      close(inputUnit)
      close(outputUnit)
      close(physicsUnit)
      close(correUnit)
    return
  end subroutine endRunTempCte

  subroutine endRunTempSwep()
    implicit none
      deallocate(part_array)
      close(inputUnit)
      close(outputUnit)
    return
  end subroutine endRunTempSwep

  subroutine endRunMag()
    implicit none
      deallocate(part_array)
      close(inputUnit)
      close(outputUnit)
    return
  end subroutine endRunMag
  ! -------------------------------    ------------------------------

  ! -------------------  CIERRA TODO LO RESTANTE  -------------------
  subroutine closeProgram()
    implicit none
      close(logUnit)
      close(socketUnitTcte)
      close(socketUnitTswep)
      close(socketUnitMag)
      close(partialUnit)
  end subroutine closeProgram
  ! -------------------------------    ------------------------------

end module p1Variables
