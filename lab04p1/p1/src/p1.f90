program Lab04P01

  use precision_interface
  use mzranmod,   only: rmzran
  use p1Variables
  use p1Mod

  implicit none
  integer   ::  i, j, kk, io, Nmeasure
  real(rp)  ::  timeInit, timeStop, timeInitFR, timeStopFR
  real(rp)  ::  E_acum, Mlocal_acum, M_acum, EE, MM
  logical   ::  calcTcte=.FALSE., calcTswep=.FALSE., calcMagnet=.FALSE.


  ! Controlar simulaciones ----------------------------------------------------
  calcTcte=.TRUE.
  ! calcTswep=.TRUE.
  ! calcMagnet=.TRUE.
  ! ---------------------------------------------------------------------------

  call cpu_time(timeInitFR)

  call initFiles()

if ( calcTcte .eqv. .FALSE. ) goto 10000
! EVOLUCION DEL SISTEMA PARA T=CTE ------------------------------------------
CicloEvol: do kk = 1, 100

  call cpu_time(timeInit)

  call readSocketTcte(io)
  if ( io<0 ) exit CicloEvol

  call initState(part_array(1:Nsites,1:Nsites), init_state_type, logunit)
  call updateBorders()

  call writeArray(part_array, outputUnit)

  ! Termalizacion -------------------------------------
  do i = 0, Ndisc, 1
    call mcStep(0)
  end do
  ! ---------------------------------------------------


  ! Medicion ------------------------------------------
  totalE  = systemEnergy(1)
  totalM  = systemMagnet(1)
  E_acum  = 0._rp
  Mlocal_acum  = 0._rp
  M_acum  = 0._rp
  EE = 0._rp
  MM = 0._rp

  corrMagN = 0._rp
  corrEneN = 0._rp

  do i = 1, Nsteps, 1

    call mcStep(1)
    ! Normalizo E y M
    totalE  = real(totalE,rp)/real(Nsites**2,rp)
    localM  = real(totalM,rp)/real(Nsites**2,rp)
    totalM  = abs(real(totalM,rp))/real(Nsites**2,rp)

    E_acum  = E_acum + totalE
    Mlocal_acum  = Mlocal_acum + localM
    M_acum  = M_acum + totalM

    ! EE  = E_acum + EE
    ! MM  = M_acum + MM

    ! call correlation()

    ! Escribo muestra
    if ( mod(i, Nsample)==0 ) then
      call writeArray(part_array, outputUnit)
      E_acum      = E_acum/real(Nsample,rp)
      Mlocal_acum = Mlocal_acum/real(Nsample,rp)
      M_acum      = M_acum/real(Nsample,rp)
      write(physicsUnit,*) i, E_acum, M_acum, Mlocal_acum
      E_acum      = 0._rp
      Mlocal_acum = 0._rp
      M_acum      = 0._rp
    end if

    ! call writePartialAdvance(part_array,partialUnit)

  end do

  ! EE  = EE/real(Nsteps,rp)
  ! MM  = MM/real(Nsteps,rp)
  !
  ! corrMagN = (corrMagN-(MM**2)/real(Nexpe,rp))/(normM2-(MM**2)/real(Nexpe,rp))
  ! corrEneN = (corrEneN-(EE**2)/real(Nexpe,rp))/(normE2-(EE**2)/real(Nexpe,rp))
  ! call writeCorrVector( reshape( [corrEneN, corrMagN], [Ncorr, 2] ), correUnit)

  call endRunTempCte()

  call cpu_time(timeStop)
  write(logUnit,*) 'Tiempo de corrida: ', timeStop-timeInit
  write(logUnit,*)  '----------------------------------------------------'
  write(logUnit,*)  ''

end do CicloEvol
! ---------------------------------------------------------------------------
10000 continue

if ( calcTswep .eqv. .FALSE. ) goto 10001
  ! BARRIDO DE TEMPERATURA ----------------------------------------------------
    cicloTempSwep: do kk = 1, 100

      call cpu_time(timeInit)

      call readSocketTswep(io)
      if (io<0) exit cicloTempSwep

      do j = 1, Nsample,1

        T_norm = dTemp(T_norm) + T_norm

        call initState(part_array(1:Nsites,1:Nsites), init_state_type, logunit)
        call updateBorders()

        call energyInit()

        ! Termalizacion -----------------------------------
        do i = 1, Ndisc, 1
          call mcStep(0)
        end do

        ! Medicion ----------------------------------------
        ! Inicializo energias
        totalM  = 0._qp;  totalM2 = 0._qp
        totalM4 = 0._qp;  meanM   = 0._qp
        totalE  = 0._qp;  totalE2 = 0._qp;  meanE = 0._qp
        specHeat    = 0._qp
        magSuscept  = 0._qp

        totalE  = systemEnergy(1);  totalM  = systemMagnet(1)
        totalE2 = systemEnergy(2);  totalM2 = systemMagnet(2)
                                    totalM4 = systemMagnet(4)

        meanE = totalE
        meanM = abs(totalM)

        Nmeasure  = 1
        do i = 1, Nsteps, 1
          call mcStep(1)

          if ( mod(i,Nmed)==0 ) then
            Nmeasure = Nmeasure + 1

            totalE2 = totalE2 + totalE**2
            totalM2 = totalM2 + totalM**2
            totalM4 = totalM4 + totalM**4

            meanE   = totalE + meanE
            meanM   = abs(totalM) + meanM
          end if
        end do

        meanE_sh  = meanE/real(Nmeasure,qp)
        meanE     = meanE_sh/real(Nsites**2,qp)
        meanE2    = totalE2/real(Nmeasure,qp)

        meanM     = meanM/real(Nmeasure,qp)
        meanM2    = totalM2/real(Nmeasure,qp)
        meanM4    = totalM4/real(Nmeasure,qp)

        specHeat = (meanE2-meanE_sh**2)/(real(Nsites**2,qp)*(T_norm*J_coup)**2)

        magSuscept = (meanM2-meanM**2)/                                       &
                        (real(J_coup*T_norm,qp)*real(Nsites**2,qp))

        binderC = (3._qp-meanM4/(meanM2**2))/2._qp
        meanM = meanM/real(Nsites**2,qp)

        write(outputUnit,'(6(Es20.8))')                                       &
                            T_norm, meanE, meanM, specHeat, magSuscept, binderC
        ! -------------------------------------------------

      end do
      call endRunTempSwep()

      call cpu_time(timeStop)
      write(logUnit,*) 'Tiempo de corrida: ', timeStop-timeInit
      write(logUnit,*)  '----------------------------------------------------'
      write(logUnit,*)  ''

    end do cicloTempSwep
  ! ---------------------------------------------------------------------------
10001 continue

if ( calcMagnet .eqv. .FALSE. ) goto 10002
  ! MEDICION DE MAGNETIZACION -------------------------------------------------
    ! Apartado (c)
    cicloMag: do kk = 1, 100

      call cpu_time(timeInit)

      call readSocketMag(io)
      if (io<0) exit cicloMag

      do j = 1, Nexpe,1

        call initState(part_array(1:Nsites,1:Nsites), init_state_type, logunit)
        call updateBorders()

        ! Termalizacion -----------------------------------
        do i = 1, Ndisc, 1
          call mcStep(0)
        end do

        ! Medicion ----------------------------------------
        ! Inicializo magnetizacion
        totalM  = 0._qp
        totalM  = systemMagnet(1)

        do i = 1, Nsteps, 1
          call mcStep(1)
          totalM = totalM/real(Nsites**2,qp)
          write(outputUnit,'(Es20.8)') totalM
        end do

        print*, 'Temperatura: ', T_norm, '| Completado: ', real(j)/real(Nexpe)*100.0, '%'
        ! -------------------------------------------------

      end do
      call endRunMag()

      call cpu_time(timeStop)
      write(logUnit,*)  'Tiempo de corrida: ', timeStop-timeInit
      write(logUnit,*)  '----------------------------------------------------'
      write(logUnit,*)  ''

    end do cicloMag
  ! ---------------------------------------------------------------------------
10002 continue

  ! CALCULO FINAL DE TIEMPO TOTAL DE CORRIDA ----------------------------------
    call cpu_time(timeStopFR)
    write(logUnit,*) 'Tiempo TOTAL de corrida: ', timeStopFR-timeInitFR
    write(logUnit,*)  '----------------------------------------------------'
    write(logUnit,*)  ''
  ! ---------------------------------------------------------------------------

  ! CIERRE DE ARCHIVO ---------------------------------------------------------
    call closeProgram()
  ! ---------------------------------------------------------------------------

end program Lab04P01
