module gaussdev

  use precision_interface
  use constantes, only: pi
  use random
  use mzranmod

  implicit none

!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
contains
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

  function gaussdev_r0(idum)
    integer   ::  idum, iset=0
    real(rp)  ::  u1, u2, fs, ang, gset, gaussdev_r0
    save  iset, gset

      if (iset == 0) then
        u1  = ran0(idum)
        u2  = ran0(idum)
        fs  = sqrt(-2._rp*log(u1))
        ang = 2._rp*pi*u2
        gset        = fs*cos(ang)
        gaussdev_r0 = fs*sin(ang)
        iset  = 1
      else
        gaussdev_r0 = gset
        iset  = 0
      end if

  end function gaussdev_r0

!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

  function gaussdev_r2(idum)
    integer   ::  idum, iset=0
    real(rp)  ::  u1, u2, fs, ang, gset, gaussdev_r2
    save  iset, gset

      if (iset == 0) then
        u1  = ran2(idum)
        u2  = ran2(idum)
        fs  = sqrt(-2._rp*log(u1))
        ang = 2._rp*pi*u2
        gset        = fs*cos(ang)
        gaussdev_r2 = fs*sin(ang)
        iset  = 1
      else
        gaussdev_r2 = gset
        iset  = 0
      end if

  end function gaussdev_r2

!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

  function gaussdev_mz()

    implicit none
    real(rp)    ::  gaussdev_mz, gset, u1, u2, fs, ang
    integer(ip) ::  iset=0
    save iset, gset

    if (iset == 0) then
      u1    = rmzran()
      u2    = rmzran()
      fs    = sqrt(-2._rp*log(u1))
      ang   = 2._rp*pi*u2
      gset  = fs*cos(ang)
      gaussdev_mz = fs*sin(ang)
      iset  = 1
    else
      gaussdev_mz = gset
      iset = 0
    endif

  end function gaussdev_mz

end module gaussdev

