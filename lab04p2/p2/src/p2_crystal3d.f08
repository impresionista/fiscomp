module p2_crystal3d

  use precision_interface
  use constantes
  implicit none
  private

  type, public :: crystal3d(NN)
    ! -------------------------------------------------------------------------
    ! Referencias de variables:
    !   Npart         : # de partículas en la muestra
    !   Npart_cryst   : # de partículas en un cristal
    !   Ncryst        : # de cristales
    !   Ncryst_side   : # de cristales por lado
    !   density       : Densidad del sistema
    !   side          : Tamaño del lado de la muestra cúbica
    !   lattice       : Tamaño de lattice del cristal
    !   volume        : Volúmen del sistema
    !   particle_mass : Masa de las partículas
    !   temperature   : Último valor calculado de temperature
    !   Ekinetic      : Último valor calculado de Ekinetic
    !   Epotential    : Último valor calculado de Epotential
    ! Métodos:
    !   GetTemperature   : Temperatura del sistema
    !   GetEkinetic      : Calcula energía cinética
    !   GetEpotential    : Calcula energía potencial
    ! -------------------------------------------------------------------------
      integer(ip), len ::  NN=32_ip
      real(rp), dimension(NN, 3)  ::  position, velocity, force
      ! integer(ip) ::  Npart, Npart_cryst, Ncryst, Ncryst_side
      ! real(rp)    ::  density, side, lattice, volume, particle_mass,          &
      !                 temperature, Ekinetic, Epotential, pressure,            &
      !                 virial_pressure
      ! real(rp), allocatable ::  cryst_struc(:,:)
      ! character(3)  :: struct_kind
      ! character(9)  :: vel_kind
      ! real(rp), private   ::  sigma   = 1._rp
      ! real(rp), private   ::  epsilon = 1._rp
      ! real(rp), private   ::  cut_rad, cut_rad2, rr6inv, Vcut
    contains
      procedure, pass(self), public ::  InitSystem
      ! procedure, pass(self), public   ::  Destroy
      ! procedure, pass(self), public   ::  EvolveStep
      ! procedure, pass(self), public   ::  GetTemperature
      ! procedure, pass(self), public   ::  GetEkinetic
      ! procedure, pass(self), public   ::  GetEpotential
      ! procedure, pass(self), public   ::  GetPressure
      ! procedure, pass(self), public   ::  GetForce
      ! procedure, pass(self), private  ::  InitCrystal
      ! procedure, pass(self), private  ::  InitDynamics
  end type crystal3d


contains

  ! subroutine InitSystem(self, struct_kind, vel_kind, density, particle_mass, temperature)
  subroutine InitSystem(self)
    class(crystal3d) ::  self
    ! real(rp), intent(in)      ::  density, particle_mass, temperature
    ! character(3), intent(in)  ::  struct_kind
    ! character(9), intent(in)  ::  vel_kind
    !   self%struct_kind    = struct_kind
    !   self%vel_kind       = vel_kind
    !   self%density        = density
    !   self%temperature    = temperature
    !   self%particle_mass  = particle_mass
    !   self%cut_rad        = cut_radius
    !
    !   self%cut_rad2    = self%cut_rad**2
    !   self%sigma       = 1._rp
    !   self%epsilon     = 1._rp
    !   self%rr6inv      = (self%sigma/self%cut_rad)**6
    !   self%Vcut        = 4._rp*self%epsilon*self%rr6inv*(self%rr6inv - 1._rp)

      ! call InitCrystal(self)
      ! call InitDynamics(self)
      ! call self%GetForce
  end subroutine InitSystem


!   subroutine InitCrystal(self)
!     ! Define tipo de estructura cristalina
!     class(crystal3d),intent(inout)  ::  self
!     real(rp)  ::  coef1 = 0._rp
!
!       select case (self%struct_kind)
!       case ("sc")
!         self%Npart_cryst = 1
!         call getCrystalParameters(self)
!         allocate(self%cryst_struc(self%Npart_cryst, 3)) !-> fila: Nro de particula
!                                                         !   columna: Coordenada
!         self%cryst_struc(1, :) = [0._rp, 0._rp, 0._rp]
!
!       case ("fcc")
!         self%Npart_cryst = 4
!         call getCrystalParameters(self)
!         allocate(self%cryst_struc(self%Npart_cryst, 3)) !-> fila: Nro de particula
!                                                         !   columna: Coordenada
!         coef1 = self%lattice/2._rp
!         self%cryst_struc(1, :) = [0._rp, 0._rp, 0._rp]
!         self%cryst_struc(2, :) = coef1*[1._rp, 1._rp, 0._rp]
!         self%cryst_struc(3, :) = coef1*[1._rp, 0._rp, 1._rp]
!         self%cryst_struc(4, :) = coef1*[0._rp, 1._rp, 1._rp]
!
!       case default
!         stop "[ERROR] Tipo de estructura cristalina inválida."
!
!       end select
!
!     return
!   end subroutine InitCrystal
!
!
!   subroutine getCrystalParameters(self)
!     class(crystal3d),intent(inout)  ::  self
!     real(rp)  ::  coef1 = 0._rp
!     ! Nota: Revisar chequeos de número correcto de partículas
!
!     if ( mod(self%Npart, self%Npart_cryst) /= 0 ) then
!       print*, '[ERROR] Npart no es divisible por ', self%Npart_cryst
!       stop
!     endif
!
!     self%Ncryst = self%Npart/self%Npart_cryst
!     ! if ( mod(self%Ncryst, 8) /= 0 ) then
!     !   print*, '[ERROR] El Nro de cristales (Ncryst) no es divisible por 8'
!     !   print*, 'Sugerencia: Aumentar el Nro de partículas en: ', self%Npart_cryst*(8-mod(self%Ncryst, 8))
!     !   print*, '            ó reducirlo en:                   ', self%Npart_cryst*(mod(self%Ncryst, 8))
!     !   stop
!     ! end if
!
!     self%side = (real(self%Npart, rp)/self%density)**(1._rp/3._rp)
!     coef1     = real(self%Ncryst, rp)**(1._rp/3._rp)
!     self%Ncryst_side = int(anint(coef1, rp), ip)
!     ! if ( int(anint(coef1)) == int(coef1, ip) ) then
!     !   print*, '[ERROR] El Nro de cristales (Ncryst) no es cúbico.'
!     !   print*, 'Sugerencia: Aumentar el Nro de partículas en: ', int(ceiling(coef1)**3)*self%Npart_cryst - self%Npart
!     !   print*, '            ó reducirlo en:                   ', self%Npart - int(floor(coef1)**3)*self%Npart_cryst
!     !   stop
!     ! end if
!
!     self%lattice  = self%side/real(self%Ncryst_side, rp)
!     self%volume   = self%side**3
!
!     ! DEBUG -----------------------------------------------
!     ! print*, "Ncryst     ", self%Ncryst
!     ! print*, "Ncryst_side", self%Ncryst_side
!     ! print*, "side       ", self%side
!     ! print*, "lattice    ", self%lattice
!     ! print*, self%Ncryst_side*self%Ncryst_side*self%Ncryst_side*self%Npart_cryst
!     ! print*, self%Npart_cryst*self%Ncryst, self%Npart
!     ! -----------------------------------------------------
!
!     return
!   end subroutine getCrystalParameters
!
!
!   subroutine InitDynamics(self)
!     class(crystal3d),intent(inout)  ::  self
!     real(rp)  ::  cryst_site(3), sumVel(3), sumVel2(3), sumVel2_mod = 0._rp, &
!                   coef1 = 0._rp, fac_normTemp = 0._rp
!     integer   ::  ii, jj, kk, ll, mm
!
!     interface
!       function dummyF()
!         use precision, only: rp => dp
!         implicit none
!         real(rp)  ::  dummyF
!       end function dummyF
!     end interface
!     procedure(dummyF), pointer  ::  ptrVelocityKind => null()
!
!     !  Elige tipo de velocidad inicial
!     select case (self%vel_kind)
!       case ("rand_uni")
!         ptrVelocityKind => uniformVelocity
!       case ("rand_mb")
!         ptrVelocityKind => mbVelocity
!       case default
!         stop "[ERROR] Tipo de velocidades iniciales inválida."
!     end select
!
!     !  Crea muestra LxL con velocidades
!     mm = 0
!     cryst_site(:) = 0._rp
!     sumVel(:)     = 0._rp
!     do ii = 0, self%Ncryst_side-1, 1
!       do jj = 0, self%Ncryst_side-1, 1
!         do kk = 0, self%Ncryst_side-1, 1
!           ! cryst_site(:) = self%lattice * [ real(ii, rp), real(jj, rp), real(kk, rp) ]
!           cryst_site(:) = self%lattice * real([ ii, jj, kk ], rp)
!           do ll = 1, self%Npart_cryst, 1
!             mm = mm+1_ip
!             self%position(mm, :) = cryst_site(:) + self%cryst_struc(ll, :)
!             self%velocity(mm, :)  = [ ptrVelocityKind(), &
!                                       ptrVelocityKind(), &
!                                       ptrVelocityKind() ]
!             sumVel(:)   = sumVel(:)  + self%velocity(mm, :)
!             sumVel2(:)  = sumVel2(:) + self%velocity(mm, :)**2
!           end do
!         end do
!       end do
!     end do
!
!     !  Renormalizacion
!     coef1 = 1._rp/real(self%Npart, rp)
!     sumVel2_mod = coef1*sum(sumVel2(:))
!     sumVel(:) = coef1*sumVel(:)
!     fac_normTemp = sqrt(3._rp*self%temperature/sumVel2_mod)
!     do ii = 1, self%Npart, 1
!       self%velocity(ii, :) = (self%velocity(ii, :) - sumVel(:))*fac_normTemp
!     end do
!
!     return
!   end subroutine InitDynamics
!
!   subroutine Destroy(self)
!     class(crystal3d),intent(inout)  ::  self
!     deallocate(self%cryst_struc)
!   end subroutine Destroy
!
!
!   subroutine EvolveStep(self, time_step, temp_ref)
!   ! -----------------------------------------------------
!   ! time_step : Paso temporal para la evolución
!   ! temp_ref  : (opcional) Temperatura de referencia.
!   !             Si esta presente termaliza el sistema a
!   !             ese valor.
!   ! -----------------------------------------------------
!     class(crystal3d)    ::  self
!     real(rp), intent(in), optional  ::  temp_ref
!     real(rp), intent(in)            ::  time_step
!     integer   ::  ii
!     real(rp)  ::  part_force_old(self%Npart, 3), new_pos(3), fact1 = 0._rp,   &
!                   tt, Ek
!
!       fact1 = .5_rp*time_step/self%particle_mass
!
!       self%position(:, :) = self%position(:, :) + time_step*(self%velocity(:, :) + fact1*self%force(:, :))
!       ! part_pos(:, :) = part_pos(:, :) - side*anint(part_pos(:, :)/side) + 0.5_rp*side
!
!       do ii = 1, self%Npart, 1
!         self%position(ii, :)  = self%position(ii, :)                                    &
!                                 - self%side*real(int(self%position(ii, :)/self%side     &
!                                 + sign([0.5_rp,0.5_rp,0.5_rp], self%position(ii, :)), ip), rp)
!       end do
!
!       part_force_old(:, :) = self%force(:, :)
!       ! call self%GetForce()
!
!       self%velocity(:, :) = self%velocity(:, :) + fact1*(part_force_old(:, :) + self%force(:, :))
!       ! self%GetTemperature()
!       ! self%GetEkinetic()
!
!       if ( present(temp_ref) .eqv. .true. ) &
!         self%velocity(:, :) = sqrt(temp_ref/self%GetTemperature())*self%velocity(:, :)
!
!     return
!   end subroutine EvolveStep
!
!
!   ! Physical properties --------------------------------------
!   function GetTemperature(self) result(temperature)
!     class(crystal3d)  ::  self
!     real(rp)          ::  temperature
!     real(rp)          ::  sumvel2
!     integer ::  ii
!       sumvel2 = 0._rp
!       do ii = 1, self%Npart, 1
!         sumvel2 = sumvel2 + sum(self%velocity(ii, :)**2)
!       end do
!       temperature    = sumvel2/(3._rp*real(self%Npart, rp))
!       self%temperature  = temperature
!   end function GetTemperature
!
!   function GetEkinetic(self) result(Ekinetic)
!     class(crystal3d)  ::  self
!     real(rp)          ::  Ekinetic
!     real(rp)          ::  sumvel2
!     integer ::  ii
!       sumvel2 = 0._rp
!       do ii = 1, self%Npart, 1
!         sumvel2 = sumvel2 + sum(self%velocity(ii, :)**2)
!       end do
!       Ekinetic = 0.5_rp*self%particle_mass*sumvel2
!       self%Ekinetic = Ekinetic
!   end function GetEkinetic
!
!   function GetEpotential(self) result(Epotential)
!     !! IMPLEMENTAR
!     class(crystal3d)  ::  self
!     real(rp)          ::  Epotential
!   end function GetEpotential
!
!   function GetPressure(self) result(pressure)
!     class(crystal3d)  ::  self
!     real(rp)          ::  pressure
!       pressure  = self%density*K_boltzman*self%GetTemperature() &
!                   + 1._rp/3._rp*self%virial_pressure/self%volume
!       self%pressure = pressure
!   end function GetPressure
!   ! ---------------------------------------------------------
!
!
!   subroutine GetForce(self)
!     class(crystal3d), intent(inout) ::  self
!     integer(ip) ::  ii, jj
!     real(rp)    ::  rr(3), frr, rr2, rr2inv
!
!     self%virial_pressure  = 0._rp
!     frr     = 0._rp
!     rr(:)   = 0._rp
!     rr2     = 0._rp
!     rr2inv  = 0._rp
!     self%rr6inv  = 0._rp
!
!     self%force  = 0._rp
!
!     do ii = 1, self%Npart-1, 1
!       do jj = ii+1, self%Npart, 1
!
!         rr(:) = self%position(ii, :) - self%position(jj, :)
!
!         ! call refitBox(rr(:))
!         ! rr(:) = rr(:) - side*anint(1._rp/side*rr(:))
!         rr(:) = rr(:) &
!                 - self%side*real( int(1._rp/self%side*rr(:)  &
!                                   +sign([0.5_rp,0.5_rp,0.5_rp], rr(:)), ip), rp)
!
!         rr2 = sum(rr(:)**2)
!         print*, rr2
!
!         ! if (rr2 <= self%cut_rad2) then
!         !   rr2inv = 1._rp/rr2
!         !   self%rr6inv = rr2inv*rr2inv*rr2inv
!         !
!         !   ! rr6inv  = rr2inv**3*sigma**6  ! rr2inv*rr2inv*rr2inv*sigma**6
!         !   ! frr = 48._rp*epsilon*rr2inv*rr6inv*(rr6inv-0.5_rp)
!         !   frr = 48._rp*rr2inv*self%rr6inv*(self%rr6inv-0.5_rp)
!         !
!         !   self%force(ii, :) = self%force(ii, :) + frr*rr(:)
!         !   self%force(jj, :) = self%force(jj, :) - frr*rr(:)
!         !
!         !   !Epot  = Epot+4._rp*epsilon*rr6inv*(rr6inv-1._rp)  ! - Vcut
!         !   self%Epotential = self%GetEpotential()+4._rp*self%rr6inv*(self%rr6inv-1._rp) - self%Vcut
!         !
!         !   self%virial_pressure = self%virial_pressure+rr2*frr
!         ! end if
!
!       end do
!     end do
!
!     return
!   end subroutine GetForce
!
!
! ! FUNCIONES GENERADORAS DE VELOCIDAD ------------------------------
!   function uniformVelocity()
!     use mzranmod, only: rmzran
!     real(rp)  ::  uniformVelocity
!     real(rp)  ::  rn
!       rn = rmzran()
!       uniformVelocity = rn-0.5_rp
!     return
!   end function uniformVelocity
!
!   function mbVelocity()
!     use gaussdev, only: gaussdev_mz
!     real(rp)  ::  mbVelocity
!       mbVelocity  = gaussdev_mz()
!     return
!   end function mbVelocity

end module p2_crystal3d
