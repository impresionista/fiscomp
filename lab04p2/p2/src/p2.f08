program lab04p2

  use precision_interface
  use p2_crystal3d


  implicit none
  ! type(crystal3d), allocatable  ::  sample
  integer ::  ii, jj, kk, Nterm, Nmed
  integer, parameter ::  pos_file=100


  Nterm = 1000
  Nmed  = 1000

  ! call sample%InitSystem(Npart=125, struct_kind="sc", vel_kind="rand_uni", density=0.8_rp, temperature=0.9_rp)
  ! call sample%InitSystem( Npart=256,            &
  !                         struct_kind="sc",     &
  !                         vel_kind="rand_uni",  &
  !                         density=0.8_rp,       &
  !                         particle_mass=1.0_rp, &
  !                         temperature=0.9_rp,   &
  !                         cut_radius=2.5_rp     &
  !                       )
  !
  ! ! call sample%GetForce()
  ! do ii = 1, 1000
  !   call sample%EvolveStep(time_step=0.005_rp, temp_ref=0.9_rp)
  ! end do

    ! ! Termalizacion ----------------------------------------
    ! do jj = 1, Nterm
    !   if (mod(jj,term_int)==0) then
    !     call systemEvolStep(1)
    !   else
    !     call systemEvolStep(0)
    !   end if
    !   ti = real(jj, rp)*time_step
    !   call writeSample()
    ! end do
    ! write (physicsUnit, *) ''
    ! write (physicsUnit, *) ''
    !
    ! ! Medicion ---------------------------------------------
    ! do jj = 1, Nmed
    !   call systemEvolStep(0)
    !   ti = real(jj+Nterm, rp)*time_step
    !   call writeSample()
    !   call writeMeansSample(jj)
    ! end do
    !
    ! call endRun()

  ! call sample%Destroy()

end program lab04p2

