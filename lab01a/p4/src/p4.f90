program lab01ap4

  use precision_interface
  use integracion

  implicit none
  integer(ip)     :: i, nn
  real(rp)        :: x0, x1, intf, exactf, error, t0, t1
  character(100)  :: time_fmt, error_fmt
  ! character, parameter :: time_fmt  = "(A14, 2X, ES16.4)"
  ! character, parameter :: error_fmt = "(A14, 2X, ES16.4)"

  open(unit=100, file="./output/stats.dat", status="replace", action="write")
  open(unit=101, file="./output/trapezoid_relative_err.dat", status="replace", action="write")
  open(unit=102, file="./output/simpson_relative_err.dat", status="replace", action="write")
  open(unit=103, file="./output/gauss_relative_err.dat", status="replace", action="write")

  x0 = 0._rp; x1 = 1._rp
  exactf = 1._rp - exp(-1._rp)
  nn = 3000

  Integration: do i = 3, nn, 2

    call int_cuadratura(f, x0, x1, i, 'trap', intf)
    error = relativeError(intf, exactf)
    write(101,*) i, intf, error

    call int_cuadratura(f, x0, x1, i, 'simp', intf)
    error = relativeError(intf, exactf)
    write(102,*) i+1, intf, error

    call int_cuadratura(f, x0, x1, i, 'gaus', intf)
    error = relativeError(intf, exactf)
    write(103,*) i, intf, error

  end do Integration

  do i = 101, 103
    close(i)
  end do

  ! format:
  time_fmt  = "'(A14, 2X, ES16.4)'"
  error_fmt = "'(A14, 2X, ES16.4)'"


  write(100,*) "# --------------------------------------------------------------"
  write(100,*) "# Estadísticas de ejecución"
  write(100,*) "# --------------------------------------------------------------"
  write(100,*) "# Error máquina: ",  epsilon(1._rp)
  write(100,*) "# --------------------------------------------------------------"

  call CPU_TIME(t0)
  call int_cuadratura(f, x0, x1, 100000000, 'trap', intf)
  call CPU_TIME(t1)
  error = relativeError(intf, exactf)
  call PrintStats(100000000, 'Trapezoid', t0, t1, error, 100)

  call CPU_TIME(t0)
  call int_cuadratura(f, x0, x1, 1001, 'simp', intf)
  call CPU_TIME(t1)
  error = relativeError(intf, exactf)
  call PrintStats(1001, 'Simpson', t0, t1, error, 100)

  call CPU_TIME(t0)
  call int_cuadratura(f, x0, x1, 10, 'gaus', intf)
  call CPU_TIME(t1)
  error = relativeError(intf, exactf)
  call PrintStats(10, 'Gauss-Legendre', t0, t1, error, 100)

! -----------------------------------------------------------------------------
contains

  function f(x)

    implicit none
    real(rp), intent(in)  :: x
    real(rp)              :: f

    f = exp(-x)

  end function f

  function relativeError(num, ex)

    ! num : valor de la integral numérica
    ! ex  : Valor de la integral exacta

    implicit none
    real(rp), intent(in)  :: num, ex
    real(rp)              :: relativeError

    relativeError = abs((num-ex)/ex)

  end function relativeError

  subroutine PrintStats(nnn, int_cuad, init_time, end_time, int_error, out_unit)
    implicit none
    integer(ip), intent(in)   :: nnn, out_unit
    character(*), intent(in)  :: int_cuad
    real(rp), intent(in)      :: init_time, end_time, int_error

    write(out_unit,*) "# Tiempo de ejecución para cuadratura ", trim(int_cuad)
    write(out_unit,'(A10, 5X, I10)') "# Puntos:", nnn
    write(out_unit,'(A14, 2X, ES16.4)') "# Tiempo (s):", end_time-init_time
    write(out_unit,'(A9, 6X, ES16.4)') "# Error:", int_error
    write(100,*) "# --------------------------------------------------------------"

    return
  endsubroutine PrintStats

end program lab01ap4
