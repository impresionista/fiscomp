\babel@toc {spanish}{}\relax 
\contentsline {chapter}{\numberline {1}Compilación y ejecución}{1}{chapter.1}%
\contentsline {chapter}{\numberline {2}Apartado 4}{2}{chapter.2}%
\contentsline {section}{\numberline {2.1}Descripción teórica}{2}{section.2.1}%
\contentsline {section}{\numberline {2.2}Análisis}{2}{section.2.2}%
\contentsline {subsection}{\numberline {2.2.1}Algoritmo del trapecio}{2}{subsection.2.2.1}%
\contentsline {subsection}{\numberline {2.2.2}Algoritmo de Simpson}{3}{subsection.2.2.2}%
\contentsline {subsection}{\numberline {2.2.3}Cuadratura de Gauss-Legendre}{3}{subsection.2.2.3}%
\contentsline {subsection}{\numberline {2.2.4}Comparación entre métodos de integración}{4}{subsection.2.2.4}%
\contentsline {chapter}{\numberline {3}Apartado 6}{5}{chapter.3}%
\contentsline {section}{\numberline {3.1}Descripción teórica}{5}{section.3.1}%
\contentsline {section}{\numberline {3.2}Análisis}{6}{section.3.2}%
