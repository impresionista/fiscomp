program lab01ap1

  use precision, only: rp=>dp, ip
  use derivacion

  implicit none
  integer :: i
  real(rp)    :: h, df_calc, ee, x0, dfx0


  open(unit=100, file="./output/error_evolution.dat", status="replace", action="write")


  x0 = 1._rp
  dfx0 = df(x0)

  do i = -1, -16, -1
    h = 10._rp**i
    call diff_centrada(f, x0, h, df_calc)
    ee = abs(dfx0-df_calc)
    write(100,*) h, ee
  end do

  close(100)


contains

  function f(x)

    use precision, only: rp=>dp, ip

    implicit none
    real(rp), intent(in)  :: x
    real(rp)              :: f

    f = exp(x)

  end function f


  function df(x)

    use precision, only: rp=>dp, ip

    implicit none
    real(rp), intent(in)  :: x
    real(rp)              :: df

    df = exp(x)

  end function df

end program lab01ap1
