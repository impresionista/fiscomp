program lab01ap6

  use precision_interface   ! Este modulo define precisión en modulos y prog
  use odesolver

  implicit none
  integer(ip)             ::  nsteps, hsteps, i, j
  integer(ip), parameter  ::  euler_file=100, rk2_file=101, rk4_file=102,     &
                              stats_file=103, err_file=104
  real(rp)                ::  t_ini, t_fin,t0, ti, x0, dx0, heu, hk2, hk4,    &
                              error, eul_error, rk2_error, rk4_error,         &
                              h_ini, hi, h_min, h_step, ti_eul, ti_rk2,       &
                              ti_rk4, energia_eul, energia_rk2, energia_rk4
  real(rp), allocatable   ::  xi(:), xi_err(:,:), errores(:),                 &
                              xi_eul(:), xi_rk2(:), xi_rk4(:)
  real(rp)                ::  time_start, time_stop, time_exec
  character(len=10)       ::  date, time
  character(len=50)       ::  dt_fmt, stats_head_fmt, eul_fmt, rk2_fmt, rk4_fmt


  ! Parametros de entrada
  t0    = 0._rp ; x0    = 1._rp   ; dx0 = 1._rp
  t_ini = 0._rp ; t_fin = 10._rp

  ! Ancho de paso de solucion usando h optimos sugeridos por la teoría
  heu = 10._rp**(-5) ! <-- Euler
  hk2 = 10._rp**(-4) ! <-- RK2
  hk4 = 10._rp**(-3) ! <-- RK4


  ! Archivos de salida
  open(euler_file,  file="./output/euler.dat",    status='replace', action="write")
  open(rk2_file,    file="./output/rk2.dat",      status='replace', action="write")
  open(rk4_file,    file="./output/rk4.dat",      status='replace', action="write")
  open(stats_file,  file="./output/stats.dat",    status='replace', action="write")
  open(err_file,    file="./output/errors.dat",   status='replace', action="write")

  ! Format definitions ----------------------------------------------------------
  dt_fmt = trim("(3(A15,X))")                     !1009
  stats_head_fmt = trim("((A12,14X,A7,3X,A13))")  !1010
  eul_fmt = trim("((A25,X,I8,X,ES16.4))")         !1011
  rk2_fmt = trim("(4(ES16.4,X))")                 !1012
  rk4_fmt = trim("((A25,X,ES16.4,X,ES16.4))")     !1013

  write(stats_file,*) " Statistics for last run:"
  call DATE_AND_TIME(DATE=date, TIME=time)
  write(stats_file,*) "----------------------------------------------------- "
  write(stats_file,dt_fmt) " Date and Time:", trim(date), trim(time)
  write(stats_file,*) "----------------------------------------------------- "
  write(stats_file,stats_head_fmt) "Method", "# Steps", "Run time"
  write(stats_file,*) "----------------------------------------------------- "


  ! Apartados a y b -----------------------------------------------------------
  allocate(xi(2))
  ! Usando Euler
  print*, 'Solución usando Euler'

    time_start = 0._rp
    call cpu_time(time_start)             !> para medir tiempo de ejecucion

    nsteps = nint((t_fin-t_ini)/heu, ip)  !> Defino numero de pasos
    xi = [ x0, dx0 ]                      !> defino paso inicial
    ti = t0
    energia_eul = energy(xi(1),xi(2))
    error = localError(xi(1), pos(ti))
    write(euler_file,*) ti, xi(1), xi(2), error, energia_eul

    do i = 1, nsteps
      call OdePasoEuler(f, ti, xi, heu)
      energia_eul = energy(xi(1),xi(2))
      error = localError(xi(1), pos(ti))
      write(euler_file,*) ti, xi(1), xi(2), error, energia_eul
    end do

    call cpu_time(time_stop)
    time_exec = time_stop-time_start      !> Tiempo de ejecución
    write(stats_file,eul_fmt) "Método de Euler:", nsteps, time_exec

  print*, 'Euler listo'
  print*, '--------------------------------------------------------------------'
  ! fin Euler

  ! Usando Runge-Kutta 2
  print*, 'Solución usando Runge-Kutta 2'

    time_start = 0._rp
    call cpu_time(time_start)             !> para medir tiempo de ejecucion

    nsteps = nint((t_fin-t_ini)/hk2, ip)  !> Defino numero de pasos
    xi = [ x0, dx0 ]                      !> defino paso inicial
    ti = t0
    energia_rk2 = energy(xi(1),xi(2))
    error = localError(xi(1), pos(ti))
    write(rk2_file,*) ti, xi(1), xi(2), error, energia_rk2

    do i = 1, nsteps
      call OdePasoRk2(f, ti, xi, hk2)
      energia_rk2 = energy(xi(1),xi(2))
      error = localError(xi(1), pos(ti))
      write(rk2_file,*) ti, xi(1), xi(2), error, energia_rk2
    end do

    call cpu_time(time_stop)
    time_exec = time_stop-time_start      !> Tiempo de ejecución
    write(stats_file,eul_fmt) "Método de Runge-Kutta 2:", nsteps, time_exec

  print*, 'Runge-Kutta 2 listo'
  print*, '--------------------------------------------------------------------'
  ! fin Runge-Kutta 2

  ! Usando Runge-Kutta 4
  print*, 'Solución usando Runge-Kutta 4'

    time_start = 0._rp
    call cpu_time(time_start)             !> para medir tiempo de ejecucion

    nsteps = nint((t_fin-t_ini)/hk4, ip)  !> Defino numero de pasos
    xi = [ x0, dx0 ]                      !> defino paso inicial
    ti = t0
    energia_rk4 = energy(xi(1),xi(2))
    error = localError(xi(1), pos(ti))
    write(rk4_file,*) ti, xi(1), xi(2), error, energia_rk4

    do i = 1, nsteps
      call OdePasoRk4(f, ti, xi, hk4)
      energia_rk4 = energy(xi(1),xi(2))
      error = localError(xi(1), pos(ti))
      write(rk4_file,*) ti, xi(1), xi(2), error, energia_rk4
    end do

    call cpu_time(time_stop)
    time_exec = time_stop-time_start      !> Tiempo de ejecución
    write(stats_file,eul_fmt) "Método de Runge-Kutta 4:", nsteps, time_exec

  print*, 'Runge-Kutta 4 listo'
  print*, '--------------------------------------------------------------------'
  ! fin Runge-Kutta 4

  deallocate(xi)
  do i = 100, 102
    close(i)
  end do
  ! ---------------------------------------------------------------------------


  ! Apartado c ----------------------------------------------------------------
  print*, 'Calculando barrido sobre h...'
  allocate(xi_eul(2), xi_rk2(2), xi_rk4(2))

  !IDEA: Paralelizar cada j en distintos cores
  h_sweep: do j = -1, -90, -1
    time_start = 0._rp
    call cpu_time(time_start)             !> para medir tiempo de ejecucion

    hi = 1.2_rp**j

    xi_eul = [ x0, dx0 ]
    xi_rk2 = [ x0, dx0 ]
    xi_rk4 = [ x0, dx0 ]

    ti_eul = t0
    ti_rk2 = t0
    ti_rk4 = t0

    nsteps  = nint((t_fin-t_ini)/hi, ip)  !> Defino numero de pasos
    SolveOde: do i = 1, nsteps
      call  OdePasoEuler(f, ti_eul, xi_eul, hi)
      call    OdePasoRk2(f, ti_rk2, xi_rk2, hi)
      call    OdePasoRk4(f, ti_rk4, xi_rk4, hi)
    end do SolveOde

    eul_error = globalError(xi_eul(1), pos(ti_eul))
    rk2_error = globalError(xi_rk2(1), pos(ti_rk2))
    rk4_error = globalError(xi_rk4(1), pos(ti_rk4))

    write(err_file,rk2_fmt) hi**(-1_ip), eul_error, rk2_error, rk4_error

    call cpu_time(time_stop)
    time_exec = time_stop-time_start      !> Tiempo de ejecución
    write(stats_file,rk4_fmt) "Barrido para h:", hi, time_exec
  end do h_sweep
  print*, 'Barrido sobre h listo'
  print*, '--------------------------------------------------------------------'

  deallocate(xi_eul, xi_rk2, xi_rk4)
  do i = 103, 104
    close(i)
  end do
  ! ---------------------------------------------------------------------------



! -----------------------------------------------------------------------------
contains

  function f(t,x)
    implicit none
    real(rp), intent(in)  ::  t, x(:)
    real(rp)              ::  f(size(x)), k
      k = 1._rp
      f(1) = x(2)       !> posicion
      f(2) = -k * x(1)  !> velocidad
  end function f

  function vel(t)
    ! Función solución para la velocidad
    implicit none
    real(rp), intent(in)  ::  t
    real(rp)              ::  vel
      vel = cos(t)-sin(t)
  end function vel

  function pos(t)
    ! Función solución para la posición
    implicit none
    real(rp), intent(in)  ::  t
    real(rp)              ::  pos
      pos = sin(t)+cos(t)
  end function pos

  function energy(x, v)
    implicit none
    real(rp), intent(in)  ::  x, v
    real(rp)              ::  k, m, energy
      k       = 1._rp
      m       = 1._rp
      energy  = 0.5_rp * ( k*x**2 + m*v**2 )
  end function energy

  function localError(aprox_value, exact_value)
    implicit none
    real(rp), intent(in)  ::  aprox_value, exact_value
    real(rp)              ::  localError
      localError =  abs(aprox_value-exact_value)
  end function localError

  function globalError(aprox_value, exact_value)
    implicit none
    real(rp), intent(in)  ::  aprox_value, exact_value
    real(rp)              ::  globalError
      globalError =  abs((aprox_value-exact_value)/exact_value)
  end function globalError

end program lab01ap6
