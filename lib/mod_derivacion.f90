module derivacion

  use precision_interface

CONTAINS

  subroutine diff_centrada(f, x0, hh, df)

    !~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    ! Derivación usando método de diferencia centrada
    ! ENTRADA:
    !   f   : funcion a derivar
    !   x0  : Punto donde valuar la derivada
    !   hh  : Ancho de integración
    ! SALIDA:
    !   df  : derivada valuada en x0
    !~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

    implicit none
    real(rp), intent(in)    ::  x0, hh
    real(rp), intent(inout) ::  df

    interface
      function f(x)
        use precision_interface
        implicit none
        real(rp), intent(in)  ::  x
        real(rp)              ::  f
      end function
    end interface

    df = (f(x0+hh)-f(x0-hh))/(2._rp*hh)

  end subroutine diff_centrada

end module
