MODULE odesolver

  use precision_interface

  implicit none

CONTAINS

  subroutine OdePasoEuler(f,ti,xi,hh)
  ! ---------------------------------------------------------
  ! Calcula solo un paso de subrutina de solucion de sistema de ODEs por
  ! metodo de Euler
  ! ENTRADA:
  !   f:  funcion a resolver (es vectorial)
  !   ti: tiempo inicial de evaluacion
  !   xi: valor de x(ti)
  !   hh: paso de evolución de la solucion
  ! SALIDA:
  !   xi: valor de x(ti+hh)
  !   ti: siguiente paso temporal
  ! ---------------------------------------------------------

    implicit none

    real(rp), intent(inout)   ::  ti, xi(:)
    real(rp), intent(in)      ::  hh

    interface
      function f(t,x)
        use precision_interface
        implicit none
        real(rp), intent(in)  ::  t, x(:)
        real(rp)              ::  f(size(x))
      end function f
    end interface

    xi(:) = xi(:) + hh*f(ti, xi(:))
    ti = ti + hh ! Esto es feo

  end subroutine OdePasoEuler


  subroutine OdePasoRk2(f, ti, xi, hh)
  ! ---------------------------------------------------------
  ! Calcula solo un paso de subrutina de solucion de sistema de ODEs por
  ! metodo de Runge-Kutta de orden 2
  ! ENTRADA:
  !   f:  funcion a resolver (es vectorial)
  !   ti: tiempo inicial de evaluacion
  !   xi: valor de x(ti)
  !   hh: paso de evolución de la solucion
  ! SALIDA:
  !   xi: valor de x(ti+hh)
  !   ti: siguiente paso temporal
  ! ---------------------------------------------------------

    implicit none

    real(rp), intent(inout)   ::  ti, xi(:)
    real(rp), intent(in)      ::  hh
    real(rp)                  ::  k1(size(xi)), k2(size(xi))

    interface
      function f(t,x)
        use precision_interface
        implicit none
        real(rp), intent(in)  ::  t, x(:)
        real(rp)              ::  f(size(x))
      end function f
    end interface

    k1(:) = hh*f(ti, xi(:))
    k2(:) = hh*f(ti+0.5_rp*hh, xi(:)+0.5_rp*k1(:))
    xi(:) = xi(:) + k2(:)
    ti = ti + hh

  end subroutine OdePasoRk2


  subroutine OdePasoRk4(f, ti, xi, hh)
  ! ---------------------------------------------------------
  ! Calcula solo un paso de subrutina de solucion de sistema de ODEs por
  ! metodo de Runge-Kutta de orden 4
  ! ENTRADA:
  !   f:  funcion a resolver (es vectorial)
  !   ti: tiempo inicial de evaluacion
  !   xi: valor de x(ti)
  !   hh: paso de evolución de la solucion
  ! SALIDA:
  !   xi: valor de x(ti+hh)
  !   ti: siguiente paso temporal
  ! ---------------------------------------------------------

    implicit none

    real(rp), intent(inout)   ::  ti, xi(:)
    real(rp), intent(in)      ::  hh
    real(rp)                  ::  k1(size(xi)), k2(size(xi)), &
                                  k3(size(xi)), k4(size(xi))

    interface
      function f(t,x)
        use precision_interface
        implicit none
        real(rp), intent(in)  ::  t, x(:)
        real(rp)              ::  f(size(x))
      end function f
    end interface

    k1(:) = hh*f(ti, xi(:))
    k2(:) = hh*f(ti+0.5_rp*hh, xi(:)+0.5_rp*k1(:))
    k3(:) = hh*f(ti+0.5_rp*hh, xi(:)+0.5_rp*k2(:))
    k4(:) = hh*f(ti+hh, xi(:)+k3(:))
    xi(:) = xi(:) + 1._rp/6._rp*(k1(:)+2._rp*k2(:)+2._rp*k3(:)+k4(:))
    ti = ti + hh

  end subroutine OdePasoRk4

END MODULE odesolver
