module constantes

    ! use precision, only: rp=>dp
    use precision_interface

    implicit none

!   CONSTANTES MATEMATICAS
    real(rp), parameter ::  pi=acos(-1._rp)

!   CONSTANTES FISICAS
    real(rp), parameter ::                                                      &
        gravity=9.80665_rp,                                                     &
        K_boltzman=1.3806488E-23,                                               &
        electron_m=9.1093837015E-31,                                            &
        electron_c=-1.602176634E-19,                                            &
        light_vel=2.99792458E8,                                                 &
        plank_const=6.62607015E-34,                                             &
        plank_bar_const=1.054571817E-34,                                        &
        bohr_mag=electron_c*plank_bar_const/(2._rp*electron_m*light_vel)

end module constantes
