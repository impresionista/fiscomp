module precision

implicit none
! Precision ENTERA
  integer, parameter  ::  ip=kind(1_4)
  integer, parameter  ::  idp=kind(1_8)


! Precision REAL
  integer, parameter  ::  sp=selected_real_kind(6), dp=selected_real_kind(13), qp=selected_real_kind(21)

end module
