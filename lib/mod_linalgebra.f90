module linalgebra

  use precision_interface

contains

  subroutine tridiag(a, d, c, b, x)
    !~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    ! Subrutina de solucion de sistema de ecuaciones lineal con matriz
    ! tridiagonal.
    ! Sistema tipo A*x = b donde a,d,c son las diagonales de A.
    ! ENTRADA:
    !   a: diagonal inferior, arreglo dimension (2:n)
    !   d: diagonal principal, arreglo dimension (1:n)
    !   c: diagonal superior, arreglo dimension (1:n-1)
    !   b: vector de datos, arreglo dimension (1:n)
    ! SALIDA:
    !   x: vector de solucion, arreglo dimension (1:n)
    !~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

    implicit none
    real(rp), intent(in)  :: a(:), d(:), c(:), b(:)
    real(rp), intent(out) :: x(:)
    real(rp), allocatable :: h(:), p(:), aa(:), dd(:), cc(:), bb(:)
    integer(ip)           :: n, n2, i

    n = ubound(d, dim=1)

    allocate(h(1:n-1), p(1:n), aa(2:n), dd(1:n), cc(1:n-1), bb(1:n))

    aa(:) = a(:)
    dd(:) = d(:)
    cc(:) = c(:)
    bb(:) = b(:)

    h(1) = cc(1)/dd(1)
    p(1) = bb(1)/dd(1)
    do i = 2, n-1, 1
      h(i) = cc(i)/(dd(i)-aa(i)*h(i-1))
      p(i) = (bb(i)-aa(i)*p(i-1))/(dd(i)-aa(i)*h(i-1))
    end do
    p(n) = (bb(n)-aa(n)*p(n-1))/(dd(n)-aa(n)*h(n-1))

    x(n) = p(n)
    do i = n-1, 1, -1
      x(i) = p(i)-h(i)*x(i+1)
    end do

    deallocate(h, p, aa, dd, cc, bb)

    return

  end subroutine tridiag

end module linalgebra
