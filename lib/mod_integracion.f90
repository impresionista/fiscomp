module integracion

  use precision_interface

CONTAINS


  subroutine int_cuadratura(f, xmin, xmax, nptos, cuadtype, ss)

    !~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    ! Subrutina de integracion por metodo de cuadraturas
    ! OJO: Alta alocación de memoria para usar las cuadratura como arreglos
    ! ENTRADA:
    !   f:  funcion a integrar
    !   xmin, xmax: valores minimos y maximos del intervalo de integracion
    !   nptos: numero de puntos de evaluacion en el intervalo de integracion
    !   cuadtype: tipo de cuadratura para la integracion
    !             "trap": trapezoide
    !             "simp": simpson
    !             "gaus": gaussiana
    ! SALIDA:
    !   ss:  valor de la integral
    !~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

    implicit none
    real(rp), intent(in)          :: xmin, xmax
    integer(ip), intent(in)       :: nptos
    character(len=4), intent(in)  :: cuadtype
    real(rp), intent(inout)       :: ss
    integer(ip)                   :: i
    real(rp)                      :: hh, xi, wi, sumpar, sumimpar
    real(rp),allocatable          :: x(:), w(:)

    interface
      function f(x)
        use precision_interface
        implicit none
        real(rp), intent(in)    ::  x
        real(rp)                ::  f
      end function
    end interface

    if ((nptos-1_ip).eq.0) stop "ERROR: intervalo de integración es cero."

    Cuadratura: select case (cuadtype)

      case ("trap")
        ! Ancho de paso
        hh = (xmax-xmin)/real(nptos-1_ip,rp)
        ! Inicio integral
        ss = 0._rp
        ! Primer paso de integracion
        ss = ss + f(xmin)*0.5_rp*hh
        ! Pasos intermedios de integracion
        do i = 1, nptos-1
          xi = xmin + real(i,rp)*hh
          ss = ss + f(xi)
        end do
        ss = ss * hh
        ! Ultimo paso de integracion
        ss = ss + f(xmax)*0.5_rp*hh
        return

      case ("simp")

        if ( mod(nptos,2_ip)==0_ip ) stop "ERROR: El numero de puntos para simpson tiene que ser IMPAR."

        ! Ancho de paso
        hh = (xmax-xmin)/real(nptos-1_ip,rp)
        ! Inicializo variables
        ss       = 0._rp
        sumpar    = 0._rp
        sumimpar  = 0._rp
        do i = 2_ip, nptos-3_ip, 2_ip
          xi        = xmin + real(i-1,rp)*hh
          sumpar    = sumpar + f(xi)
          sumimpar  = sumimpar + f(xi+hh)
        end do
        ! Ultimo paso par
        sumpar = sumpar + f(xmin + real(nptos-2_ip,rp)*hh) ! pq nptos-2?
        ! sumpar = sumpar + f(xmin + real(nptos-1_ip,rp)*hh)

        ss = hh * (f(xmin) + 4._rp*sumpar + 2._rp*sumimpar + f(xmax)) / 3._rp

        return

        ! ~~~~~~~~~~~~~~~~~~~~~~~ PRUEBAS ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        ! -------------- Alternativa con array - FUNCIONA ---------------------
        ! allocate(w(1:nptos))
        ! do i = 2, nptos-1
        !   w(i) = ( 2._rp + real( (1_ip+(-1_ip)**(i)), rp) ) * hh
        ! end do
        ! w(1) = hh
        ! w(nptos) = hh
        ! w(:) = w(:)/3._rp
        !
        ! ss = 0._rp
        ! do i = 1, nptos
        !   xi = xmin + real((i-1_ip),rp)*hh
        !   ss = ss + f(xi)*w(i)
        ! end do
        ! deallocate(w)
        ! return
        ! ~~~~~~~~~~~~~~~~~~~~~~~ FIN PRUEBAS ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

      case ("gaus")

        allocate(x(0:nptos), w(0:nptos))
        call gauss(nptos, 0, xmin, xmax, x(0:nptos), w(0:nptos))

        ss = 0._rp
        do i = 0, nptos
          ss = ss + f(x(i))*w(i)
        end do

        deallocate(w)
        deallocate(x)

        return

    end select Cuadratura


  end subroutine int_cuadratura

!###############################################################################

  subroutine gauss(npts, job, a, b, x, w)

    !~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    ! Proporcionada por la Cátedra
    !~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    ! Subrutina que calcula los nodos y pesos para integración por
    ! cuadratura gausiana
    ! Documentado en libro de Landau.
    !~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

    implicit none
    integer, intent(in)     :: npts,job
    real (rp), intent(in)   :: a,b
    real (rp), intent(out)  :: x(0:npts),w(0:npts)
    real (rp)               :: t,t1,pp,p1,p2,p3,eps
    real (rp)               :: pi
    real (rp)               :: xi
    integer                 :: m,i,j
    !
    pi = acos(-1._rp)
    m = 0; i = 0; j = 0
    t = 0._rp;  t1 = 0._rp; pp = 0._rp;
    p1 = 0._rp; p2 = 0._rp; p3 = 0._rp
    eps = 3.e-14_rp   ! Accuracy: ********ADJUST THIS*********!
    m = (npts + 1)/2
    ! do i=1,m + 1
    do i=1,m + 1
      ! t = cos(pi*(dble(i) - 0.25_rp)/(dble(npts) + 0.5_rp) )
      t = cos(pi*(dble(i) - 0.25_rp)/(dble(npts+1) + 0.5_rp) )
      t1 = 1._rp
      do while( (abs(t - t1) ) >= eps)
        p1 = 1._rp ; p2 = 0._rp
        !
        do j=1,npts + 1
          p3 = p2; p2 = p1
          ! p1 = ( (2._rp*dble(j) - 1._rp)*t*p2 - (dble(j) - 1._rp)*p3)/(dble(j) )
          p1 = ( (2._rp*dble(j) - 1._rp)*t*p2 - dble(j-1)*p3)/(dble(j) )
        enddo
        !
        ! pp = dble(npts)*(t*p1 - p2)/(t*t - 1._rp)
        pp = dble(npts+1)*(t*p1 - p2)/(t*t - 1._rp)
        t1 = t
        t = t1 - p1/pp
      enddo
      x(i - 1) = - t;
      ! x(npts - i) = t
      x(npts + 1 - i) = t
      w(i - 1) = 2._rp/( (1._rp - t*t)*pp*pp)
      ! w(npts - i) = w(i - 1)
      w(npts + 1 - i) = w(i - 1)
      ! print *," x(i - 1)", x(i - 1) , " w " , w(npts - i),i
    enddo
    if (job == 0) then
      do i=0, npts
        x(i) = x(i)*(b - a)/2._rp + (b + a)/2._rp
        w(i) = w(i)*(b - a)/2._rp
      enddo
    endif
    if (job == 1) then
      do i=0, npts
        xi = x(i)
        x(i) = a*b*(1._rp + xi) / (b + a - (b - a)*xi)
        w(i) = w(i)*2._rp*a*b*b/( (b + a - (b - a)*xi)*(b + a - (b - a)*xi) )
      enddo
    endif
    if (job == 2) then
      do i=0, npts
        xi = x(i)
        x(i) = (b*xi + b + a + a) / (1._rp - xi)
        w(i) = w(i)*2.*(a + b)/( (1._rp - xi)*(1. - xi) )
      enddo
    endif
    !
    return

  end subroutine gauss

!###############################################################################

  subroutine integrate_mc(f, xmin, xmax, nptos, rand, ss, err)

    !~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    ! Subrutina de integracion por metodo de monte carlo.
    ! ENTRADA:
    !   f:      funcion a integrar
    !   xmin, xmax: valores minimos y maximos del intervalo de integracion
    !   nptos:  numero de puntos de evaluacion en el intervalo de integracion
    !   rand:   generador de numeros a usar:
    !       mz: mzran
    !       mt: Mersene-Twister
    ! SALIDA:
    !   ss:  valor de la integral
    !   err: error asociado a ss
    !~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

    use mtmod,    only: grnd
    use mzranmod, only: rmzran

    implicit none
    real(rp), intent(in)          :: xmin, xmax
    integer(ip), intent(in)       :: nptos
    character(len=2), intent(in)  :: rand
    real(rp), intent(inout)       :: ss, err
    integer(ip) ::  i, j, k
    real(rp)    ::  factor, dif, xn

    interface
      function f(x)
        use precision_interface
        implicit none
        real(rp), intent(in)    ::  x
        real(rp)                ::  f
      end function f
    end interface

      factor  = (xmax-xmin)/real(nptos,rp)
      dif     = (xmax-xmin)
      ss  = 0._rp
      err = 0._rp

      select case (rand)
      case ("mz")
        do i  = 1, nptos, 1
          xn  = xmin+grnd()*dif
          ss  = ss + f(xn)
          err = err + f(xn)**2
        end do

      case ("mt")
        do i  = 1, nptos, 1
          xn  = xmin+rmzran()*dif
          ss  = ss + f(xn)
          err = err + f(xn)**2
        end do

      case default
        print*, 'ERROR: ', rand, ' no es un metodo de generacion de numeros random válido.'
        stop
      end select

      err = dif * sqrt( ( err/real(nptos,rp) - (ss/real(nptos,rp))**2 ) / real(nptos,rp) )
      ss  = factor*ss

      return

  end subroutine integrate_mc

!###############################################################################

  subroutine integrate_mc_sampling(f, samp, samp_1, nptos, rand, ss, err)

    !~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    ! Subrutina de integracion por metodo de monte carlo con important
    ! sampling para funciones de sampleo normalizadas entre xmin y xmax.
    ! ENTRADA:
    !   f:      funcion a integrar
    !   samp:   Funcion que implementa sampleo
    !   samp_1: Funcion inversa de la probabilidad acumulada de samp
    !   xmin, xmax: valores minimos y maximos del intervalo de integracion
    !   nptos:  numero de puntos de evaluacion en el intervalo de integracion
    !   rand:   generador de numeros a usar:
    !       mz: mzran
    !       mt: Mersene-Twister
    ! SALIDA:
    !   ss:   valor de la integral
    !   err:  error asociado a ss
    !~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

    use mtmod,    only: grnd
    use mzranmod, only: rmzran

    implicit none
    integer(ip), intent(in)       :: nptos
    character(len=2), intent(in)  :: rand
    real(rp), intent(inout)       :: ss, err
    integer(ip) ::  i, j, k
    real(rp)    ::  urand, xn

    interface
      function f(x)
        use precision_interface
        implicit none
        real(rp), intent(in)    ::  x
        real(rp)                ::  f
      end function f
      function samp(x)
        use precision_interface
        implicit none
        real(rp), intent(in)    ::  x
        real(rp)                ::  samp
      end function samp
      function samp_1(x)
        use precision_interface
        implicit none
        real(rp), intent(in)    ::  x
        real(rp)                ::  samp_1
      end function samp_1
    end interface

      ss  = 0._rp
      err = 0._rp

      select case (rand)
      case ("mz")
        do i  = 1, nptos, 1
          urand = samp_1(grnd())
          ss    = ss + f(urand)/samp(urand)
          err   = err + (f(urand)/samp(urand))**2
        end do

      case ("mt")
        do i  = 1, nptos, 1
          urand = samp_1(rmzran())
          ss    = ss + f(urand)/samp(urand)
          err   = err + (f(urand)/samp(urand))**2
        end do

      case default
        print*, 'ERROR: ', rand, ' no es un metodo de generacion de numeros random válido.'
        stop
      end select

      err = sqrt((err/real(nptos,rp)-(ss/real(nptos,rp))**2)/real(nptos,rp))
      ss  = ss/real(nptos,rp)

      return

  end subroutine integrate_mc_sampling


!###############################################################################

  ! subroutine int_trapecio(f, xmin, xmax, nptos, sum)
  !
  !   !~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  !   ! Subrutina de integracion por metodo de trapecio
  !   ! ENTRADA:
  !   !   f:  funcion a integrar
  !   !   xmin, xmax: valores minimos y maximos del intervalo de integracion
  !   !   nptos: numero de puntos de evaluacion en el intervalo de integracion
  !   ! SALIDA:
  !   !   sum:  valor de la integral
  !   !~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  !
  !   implicit none
  !   real(rp), intent(in)    :: xmin, xmax
  !   integer(ip), intent(in) :: nptos
  !   real(rp), intent(inout) :: sum
  !   integer(ip)             :: i
  !   real(rp)                :: hh, xi, wi
  !
  !   interface
  !     function f(x)
  !       use precision, only:  rp=>dp
  !       implicit none
  !       real(rp), intent(in)    ::  x
  !       real(rp)                ::  f
  !     end function
  !   end interface
  !
  !
  !   ! Ancho de paso
  !   hh = (xmax-xmin)/real(nptos-1_ip,rp)
  !
  !   ss = 0._rp
  !
  !   ! Primer paso de integracion
  !   xi = xmin
  !   wi = 0.5_rp*hh
  !   ss = ss + f(xi)*wi
  !
  !   wi = hh
  !   do i = 2, nptos-1
  !     xi = xi + hh
  !     ss = ss + f(xi)*wi
  !   end do
  !
  !   ! Ultimo paso
  !   wi = 0.5_rp*hh
  !   ss = ss + f(xi)*wi
  !
  ! end subroutine int_trapecio

end module integracion
