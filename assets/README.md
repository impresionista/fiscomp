# Referencia de uso:

Para compilar basta ejecutar `./makefile.sh`. La salida del programa estará en
`./bin`. El nombre del ejecutable principal se corresponde con el nombre del
directorio del programa. Ante dudas revisar el archivo `makefile.sh` que
documenta el proceso exacto de compilación.


## Referencia de directorios:

- Codigo fuente:          `./src`
- Librerias generales:    `./lib`
- Programa compilado:     `./bin`
- Imagenes y graficos:    `./img`
- Entradas al programa:   `./input`
- Salidas del programa:   `./output`
