module GlobalVariables

! -----------------------------------------------------------------------------
! Contiene la definicion de las variables globales necesarias para la ejecución
! del programa.
! -----------------------------------------------------------------------------

  use precision_interface
  use constantes, only: pi
  use, intrinsic :: iso_c_binding

  implicit none
  include "/usr/include/fftw3.f03"
  integer(ip)             ::  nptos, Nxinit, Nener, Nite_ener, nptos1, nptos2
  real(rp)                ::  t0, ti, tf, ht, factor, he, p2j, p2max, thetaold,&
                              x0(4), xi(4), p0(2), pj(2),                      &
                              theta1_min, theta1_max, theta2_min, theta2_max,  &
                              theta1i, theta2j, h1, h2, ti_old,                &
                              alpha, beta, gama, m1, l1,                       &
                              time_start, time_stop
  real(rp), allocatable   ::  energy(:)

  type(C_ptr)             ::  plan_r2c, inptr1, outptr1, inptr2, outptr2
  integer(C_size_T)       ::  insignalsize, outsignalsize
  real(C_double), pointer             ::  signalin1(:), signalin2(:)
  complex(C_double_complex), pointer  ::  signalout1(:), signalout2(:)

  integer, parameter      ::  evolfile=100, heymapfile=101, powerfile=102,     &
                              energyfile=103
  character(len=30),parameter ::  fmt_evol="(ES22.6, 4(2X, ES22.6))",          &
                                  fmt_poin="(ES22.6, 2X, ES22.6)",             &
                                  fmt_pow="(3(2X, ES22.6))",                   &
                                  fmt_hey="(3(2X, ES22.6))"

! -----------------------------------------------------------------------------

contains

  subroutine FFTW_AllocateVariables()
    ! FFTW3:
    ! - Alocar usando fftw_alloc (ver página 88 de pdf)
    implicit none
    inptr1   = fftw_alloc_real(int(insignalsize, C_SIZE_T))
    outptr1  = fftw_alloc_complex(int(outsignalsize, C_SIZE_T))
    call c_f_pointer(inptr1,   signalin1,   [insignalsize])
    call c_f_pointer(outptr1,  signalout1,  [outsignalsize])
    inptr2   = fftw_alloc_real(int(insignalsize, C_SIZE_T))
    outptr2  = fftw_alloc_complex(int(outsignalsize, C_SIZE_T))
    call c_f_pointer(inptr2,   signalin2,   [insignalsize])
    call c_f_pointer(outptr2,  signalout2,  [outsignalsize])
  end subroutine FFTW_AllocateVariables

  subroutine FFTW_DeallocateVariables()
    implicit none
    call fftw_free(inptr1)
    call fftw_free(outptr1)
    call fftw_free(inptr2)
    call fftw_free(outptr2)
  end subroutine FFTW_DeallocateVariables


  subroutine OpenFiles()
    implicit none
    integer   ::  ios
    open(unit=evolfile,   file="./output/evolution.dat",      iostat=ios, status="replace", action="write")
    if ( ios /= 0 ) stop "Error abriendo archivo de salida ./output/evolution.dat"
    open(unit=powerfile,  file="./output/power_spectrum.dat", iostat=ios, status="replace", action="write")
    if ( ios /= 0 ) stop "Error abriendo archivo de salida ./output/power_spectrum.dat"
  end subroutine OpenFiles

  subroutine CloseFiles()
    implicit none
    close(evolfile)
    close(powerfile)
  end subroutine CloseFiles


  subroutine OpenHeymapFile()
    implicit none
    integer   ::  ios
    open(unit=heymapfile, file="./output/heymap.dat",         iostat=ios, status="replace", action="write")
    if ( ios /= 0 ) stop "Error abriendo archivo de salida ./output/heymap.dat"
  end subroutine OpenHeymapFile

  subroutine CloseHeymapFile()
    implicit none
    close(heymapfile)
  end subroutine CloseHeymapFile


  subroutine OpenEnergyFile(e_val)
    implicit none
    real(rp), intent(in)  ::  e_val
    character(len=80)     ::  filename, energy_value
    integer   ::  ios
    write(energy_value,"(ES16.3)") e_val
    energy_value  = trim(adjustl(energy_value))
    filename      = "./output/poincare_map_" // trim(energy_value) // ".dat"
    open(unit=energyfile,  file=filename, iostat=ios, status="replace", action="write")
    if ( ios /= 0 ) stop "Error abriendo archivo de salida "//filename
  end subroutine OpenEnergyFile

  subroutine CloseEnergyFile()
    implicit none
    close(energyfile)
  end subroutine CloseEnergyFile

end module GlobalVariables
