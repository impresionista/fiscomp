module p3_functions

  use precision_interface
  use GlobalVariables
  private
  public Pendulo, p_init, p2_max, P2Theta, Theta2P

contains

  function Pendulo(t, x)
    implicit none
    real(rp), intent(in)  ::  t, x(:)
    real(rp)  ::  x3x3, x4x4, sinx1, sinx2, sinx1x2, &
                  cosx1, cosx2, cosx1x2, fact1
    real(rp)  ::  Pendulo(size(x))

    x3x3    = x(3)*x(3)
    x4x4    = x(4)*x(4)
    sinx1   = sin(x(1))
    sinx2   = sin(x(2))
    sinx1x2 = sin(x(1)-x(2))
    cosx1   = cos(x(1))
    cosx2   = cos(x(2))
    cosx1x2 = cos(x(1)-x(2))
    fact1   = 1._rp/(1._rp+alpha*sinx1x2**2)

    Pendulo(1) = x(3)
    Pendulo(2) = x(4)
    Pendulo(3) = fact1* (                                           &
                          -(1._rp+alpha)*gama*sinx1 -               &
                          alpha*( beta*x4x4*sinx1x2 +               &
                                  cosx1x2*(x3x3*sinx1x2-gama*sinx2) &
                                )                                   &
                        )
    Pendulo(4) =  (1._rp/beta)*fact1*(                                         &
                                      (1._rp+alpha)*(x3x3*sinx1x2-gama*sinx2)+ &
                                      cosx1x2*((1._rp+alpha)*gama*sinx1 +      &
                                                alpha*beta*x4x4*sinx1x2        &
                                              )                                &
                                     )
  end function Pendulo


  subroutine p_init(p2, x, E, p)
    !
    implicit none
    real(rp), intent(in)    :: p2, x(:), E
    real(rp), intent(inout) :: p(2)
    real(rp)                :: cte1, fact1, fact2

    cte1  = m1*l1*l1

    fact1 = (2._rp+alpha*(1._rp-cos(2._rp*(x(1)-x(2)))))/(2._rp*alpha)
    fact2 = 2._rp*beta*beta*alpha*(                                            &
                                    E/cte1 + gama * (                          &
                                                      (1._rp+alpha)*cos(x(1))+ &
                                                      beta*alpha*cos(x(2))     &
                                                    )                          &
                                  ) - (p2*p2)/(cte1*cte1)

    p(1)  = 1._rp/beta * ( p2/cte1 + sqrt(fact1*fact2) )
    p(2)  = p2
  end subroutine p_init


  function p2_max(E, x)
    implicit none
    real(rp), intent(in)  :: E, x(4)
    real(rp)              :: p2_max
    real(rp)              :: fact1

    fact1 = 1._rp+alpha

    p2_max = sqrt(  2._rp*alpha*beta*beta*                       &
                    (1._rp+alpha*sin(x(2))*sin(x(2)))/fact1 *    &
                    (E + fact1*gama + alpha*beta*gama*cos(x(2))) &
                  )
  end function p2_max


  subroutine P2Theta(p, x, E)
    ! Funcion que toma p1, p2, x, E y devuelve diff(theta1) y diff(theta2).
    implicit none
    real(rp), intent(in)    :: p(2), E
    real(rp), intent(inout) :: x(4)
    real(rp)                :: sinx1x2, cosx1x2, coef1

    sinx1x2 = sin(x(1)-x(2))
    cosx1x2 = cos(x(1)-x(2))

    coef1   = ( p(1)/(m1*l1) - p(2)*cosx1x2/(beta*m1*l1*l1) ) / &
              ( 1._rp + alpha*sinx1x2*sinx1x2 )

    x(1)  = x(1)
    x(2)  = x(2)
    x(3)  = coef1
    x(4)  = p(2)/(alpha*beta*beta*m1*l1*l1) - coef1*cosx1x2/beta
  end subroutine P2Theta


  subroutine Theta2P(p, x, E)
    ! Funcion que toma posición (x) y energía (E) y devuelve momentos (p1, p2).
    use constantes
    implicit none
    real(rp), intent(in)  :: x(4), E
    real(rp), intent(out) :: p(2)
    real(rp)              :: sinx1x2, cosx1x2, coef1

    sinx1x2 = sin(x(1)-x(2))
    cosx1x2 = cos(x(1)-x(2))

    coef1 = m1*l1*l1*alpha*beta*beta * ( x(4) + x(3)*cosx1x2/beta )

    p(1)  = x(3)*m1*l1*l1*(1._rp+alpha*sinx1x2*sinx1x2) + &
            (coef1*cosx1x2/beta)
    p(2)  = coef1
  end subroutine Theta2P


end module p3_functions
