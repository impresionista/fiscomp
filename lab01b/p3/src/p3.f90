program lab01b_program3

  use odesolver
  use constantes, only: pi

  use precision_interface
  use GlobalVariables
  use p3_functions

  implicit none
  integer   ::  i, j, k
  LOGICAL   ::  run_evol=.FALSE., run_poincare=.FALSE., run_hay=.FALSE.

  ! NOTAS: --------------------------------------------------------------------
  ! x0  = [ theta1, theta2, dtheta1, dtheta2 ]
  ! ---------------------------------------------------------------------------

  ! Variables globales --------------------------------------------------------
  run_evol=.TRUE.      !> Ejecuta evolución y mapa de potencias si es TRUE
  run_poincare=.TRUE.  !> Ejecuta cálculo de mapa de poincaré si es TRUE
  run_hay=.TRUE.       !> Ejecuta cálculo de mapa de Hay si es TRUE
  ! ---------------------------------------------------------------------------
  if (                                  &
    (run_evol .eqv. .FALSE.) .and.      &
    (run_poincare .eqv. .FALSE.) .and.  &
    (run_hay .eqv. .FALSE.)             &
  ) STOP "No se definió ningun tipo de corrida."


  ! Evolucion del pendulo (a) y espectro de potencias (c) ---------------------
    if (run_evol .eqv. .FALSE.) goto 10000 !> Evita evolución y espectro
    ! Parametros de ecuación -------------------------------
    alpha = 1._rp/3._rp
    beta  = 0.5_rp
    gama  = 0.5_rp
    m1    = 1._rp
    l1    = 1._rp
    ! Condiciones iniciales para caso bien comportado -------
    t0  = 0._rp           !> Tiempo inicial en segundos
    tf  = 450._rp         !> Tiempo final en segundos
    ht  = (10._rp)**(-3)  !> Ancho de paso temporal
    x0  = [ 0._rp, 0._rp, 0.332_rp, 0.845_rp ]

    ! Inicializo variables para evolucion
    xi(:) = x0(:)
    ti    = t0
    nptos = int((tf-t0)/ht)

    call OpenFiles()

    print*, '-----------------------------------------------------------------'
    print*, 'Calculando evolucion y espectro de potencias del pendulo ...'

    ! Calcula espectro de potencias (c)
    insignalsize  = nptos
    outsignalsize = 1+nptos/2
    call FFTW_AllocateVariables()
    plan_r2c = fftw_plan_dft_r2c_1d(nptos, signalin1, signalout1, FFTW_MEASURE)

    signalin1(:) = 0._rp
    signalin2(:) = 0._rp
    PendulumEvolution_a: do i = 1, nptos
      signalin1(i) = xi(1)
      signalin2(i) = xi(2)
      if (mod(i,10)==0 ) write(evolfile,fmt_evol) ti, xi
      call OdePasoRk4(Pendulo, ti, xi, ht)
    end do PendulumEvolution_a
    write(evolfile,*)
    write(evolfile,*)

    call fftw_execute_dft_r2c(plan_r2c, signalin1, signalout1)
    call fftw_execute_dft_r2c(plan_r2c, signalin2, signalout2)

    factor = 1._rp/(tf-t0)
    NonChaosRenorm_c: do j = -nptos/2, nptos/2
      if ( j < 0 ) then
        write(powerfile,fmt_pow) factor*real(j,rp),       &
          abs(conjg(signalout1(-j+1))/real(nptos,rp))**2, &
          abs(conjg(signalout2(-j+1))/real(nptos,rp))**2
      else
        write(powerfile,fmt_pow) factor*real(j,rp),       &
          abs(signalout1(j+1)/real(nptos,rp))**2,         &
          abs(signalout2(j+1)/real(nptos,rp))**2
      endif
    end do NonChaosRenorm_c
    write(powerfile,*)
    write(powerfile,*)


    ! Condiciones iniciales para caso caotico ---------------
    t0  = 0._rp           !> Tiempo inicial en segundos
    tf  = 450._rp         !> Tiempo final en segundos
    ht  = (10._rp)**(-3)  !> Ancho de paso temporal
    x0  = [ 0._rp, 0._rp, sqrt(1.125_rp), 0._rp ]

    ! Inicializo variables para evolucion
    xi(:) = x0(:)
    ti    = t0
    nptos = int((tf-t0)/ht)

    signalin1(:) = 0._rp
    signalin2(:) = 0._rp
    ChaosEvolution_a: do i = 1, nptos
      signalin1(i) = xi(1)
      signalin2(i) = xi(2)
      if (mod(i,10)==0 ) write(evolfile,fmt_evol) ti, xi
      call OdePasoRk4(Pendulo, ti, xi, ht)
    end do ChaosEvolution_a

    call fftw_execute_dft_r2c(plan_r2c, signalin1, signalout1)
    call fftw_execute_dft_r2c(plan_r2c, signalin2, signalout2)

    ! Calcula espectro de potencias (c)
    factor = 1._rp/(tf-t0)
    ChaosRenorm_c: do j = -nptos/2, nptos/2
      if ( j < 0 ) then
        write(powerfile,fmt_pow) factor*real(j,rp),       &
          abs(conjg(signalout1(-j+1))/real(nptos,rp))**2, &
          abs(conjg(signalout2(-j+1))/real(nptos,rp))**2
      else
        write(powerfile,fmt_pow) factor*real(j,rp),       &
          abs(signalout1(j+1)/real(nptos,rp))**2,         &
          abs(signalout2(j+1)/real(nptos,rp))**2
      endif
    end do ChaosRenorm_c

    print"(2X, A30)", 'Evolución y espectro: OK'

    call FFTW_DeallocateVariables()
    call fftw_destroy_plan(plan_r2c)
    call CloseFiles()
    10000 continue !> Evita evolución y espectro de potencias para no sobreescribir
  ! ---------------------------------------------------------------------------


  ! Mapa de Poincare (b) ------------------------------------------------------
    if (run_poincare .eqv. .FALSE.) goto 10001 !> Evita mapa de poincaré para no sobreescribirlo
    ! Parametros para la simulacion -------------------------
    t0        = 0._rp           !> Tiempo inicial en segundos
    tf        = 10000._rp       !> Tiempo final en segundos
    ht        = (10._rp)**(-3)  !> Ancho de paso temporal
    Nite_ener = 50              !> Nro de cond iniciales de p1 a barrer por cada energia
    Nener     = 5               !> Cantidades de energias a barrer
    ! Parametros de ecuación -------------------------------
    alpha     = 1._rp/3._rp
    beta      = 0.5_rp
    gama      = 0.5_rp
    m1        = 1._rp
    l1        = 1._rp

    print*, '-----------------------------------------------------------------'
    print*, 'Calculando mapa de poincaré ...'

    allocate(energy(Nener))

    x0      =  [ 0._rp, 0._rp, 0._rp, 0._rp ]
    energy  =  [ -0.745_rp, -0.6_rp, -0.58_rp, -0.575_rp, 0._rp ]

    nptos = int((tf-t0)/ht, ip)

    Energy_sweep_b: do i = 1, nener

      ! Inicializo variables para evolucion
      p2max = p2_max(energy(i), x0)
      he    = 2._rp*p2max/real(nite_ener,rp)

      call OpenEnergyFile(energy(i))

      P2Sweep: do j = 0, nite_ener

        xi    = x0
        p2j   = -p2max+real(j,rp)*he
        call p_init(p2j, xi, energy(i), p0)
        call P2Theta(p0 , xi, energy(i))

        ! Imprime porcentaje de avance
        if ( mod(i*j,10)==0 ) then
          write(*,"(2X, A20, X, F8.2, A14)") "Mapa de poincaré:", &
            real((i-1)*nite_ener+j)/real(nener*nite_ener)*100._rp, " % completado"
        endif

        ti = t0
        SistemEvol_b: do k = 0, nptos
          thetaold  = xi(1)
          call OdePasoRk4(Pendulo, ti, xi, ht)
          if (thetaold<0._rp .and. xi(1)>0._rp) then
            call Theta2P(pj, xi, energy(i))
            write(energyfile,fmt_poin) xi(2), pj(2)
          end if
        end do SistemEvol_b

        write(energyfile,*) ''
        write(energyfile,*) ''

      end do P2Sweep

      call CloseEnergyFile()

    end do Energy_sweep_b

    print"(2X, A30)", 'Mapa de poincaré: OK'

    deallocate(energy)
    10001 continue !> Evita mapa de poincaré para no sobreescribirlo
  ! ---------------------------------------------------------------------------


  ! Mapa de Hay (d) -----------------------------------------------------------
    if (run_hay .eqv. .FALSE.) goto 10002 !> Evita mapa de Hay para no sobreescribirlo
    ! Parametros para la simulacion -------------------------
    t0      = 0._rp           !> Tiempo inicial en segundos
    tf      = 10000._rp       !> Tiempo maximo en segundos
    ht      = (10._rp)**(-3)  !> Ancho de paso temporal
    nptos1  = 600             !> Nro de ptos para calcular theta1
    nptos2  = 300             !> Nro de ptos para calcular theta2
    theta1_min = -3.0_rp ; theta1_max = 3.0_rp
    theta2_min = 0.0_rp  ; theta2_max = 3.0_rp
    ! Parametros de ecuación -------------------------------
    alpha   = 1.0_rp
    beta    = 1.0_rp
    gama    = 1.0_rp
    m1      = 1.0_rp
    l1      = 1.0_rp

    h1 = (theta1_max-theta1_min)/real(nptos1, rp) ! Ancho de paso para theta1
    h2 = (theta2_max-theta2_min)/real(nptos2, rp) ! Ancho de paso para theta2

    print*, '-----------------------------------------------------------------'
    print*, 'Calculando mapa de Hey ...'

    call OpenHeymapFile()
    call cpu_time(time_start)

    Theta1Sweep: do i = 0, nptos1-1

      theta1i = real(i,rp)*h1 + theta1_min

      Theta2Sweep: do j = 0, nptos2-1

        theta2j = real(j,rp)*h2 + theta2_min

        ! Inicializo variables para evolucion
        ti  = t0
        xi  = [ theta1i, theta2j, 0._rp, 0._rp ]

        Check_NoFlip: if ( 2._rp*cos(xi(1))+cos(xi(2)) > 1._rp ) then
          ti  = tf
        else
          Evol_Hay: do while ( ti < tf )
            call OdePasoRk4(Pendulo, ti, xi, ht)
            if ( (abs(xi(1))>pi) .or. (abs(xi(2))>pi) ) exit Check_NoFlip
          end do Evol_Hay
        end if Check_NoFlip

        ! Para escribir en pantalla un porcentaje de avance
        if ( mod(i*j,1000)==0 ) then
          write(*,"(2X, A23, X, F8.2)") "Porcentaje completado: ", &
            real(i*nptos2+j)/real(nptos1*nptos2)*100._rp
        endif

        ! Escribo 'matriz' con valores temporales en arrelo de nptos1 x nptos2
        write(heymapfile,fmt_hey) theta1i, theta2j, ti

      end do Theta2Sweep

      write(heymapfile,*) ''
      write(heymapfile,*) ''

    end do Theta1Sweep

    call cpu_time(time_stop)
    call CloseHeymapFile()

    print*, 'Tiempo de CPU (en horas): ', (time_stop-time_start)/60._rp/60._rp

    print"(2X, A30)", 'Mapa de Hey: OK'
    10002 continue !> Evita mapa de Hay para no sobreescribirlo
  ! ---------------------------------------------------------------------------

end program lab01b_program3
