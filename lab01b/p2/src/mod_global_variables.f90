module GlobalVariables

! -----------------------------------------------------------------------------
! Contiene la definicion de las variables globales necesarias para la ejecución
! del programa.
! -----------------------------------------------------------------------------

  use precision_interface
  use, intrinsic :: iso_c_binding

  implicit none
  include "/usr/include/fftw3.f03"
  integer(ip)             ::  Nr, Nxini, Nsteps, Ntrans, Nsignal
  real(rp)                ::  rmin, rmax, x0, xsignal, factor, xmin_zoom,     &
                              xmax_zoom, xmin, xmax
  real(rp), allocatable   ::  r(:), xini(:), xt(:), histogram(:,:)

  type(C_ptr)             ::  plan_r2c, inptr, outptr
  integer(C_size_T)       ::  insignalsize, outsignalsize
  real(C_double), pointer             ::  signalin(:)
  complex(C_double_complex), pointer  ::  signalout(:)

  integer, parameter      ::  statsfile=100, evolfile=101, xinifile=102,      &
                              rfile=103, signalfile=104, histfile=105,        &
                              orbitsfile=106, zoomfile=107, lyapfile=108

contains

  subroutine AllocateVariables()
    implicit none
    allocate( r(Nr), xini(Nxini), xt(Nxini) )
  end subroutine AllocateVariables

  subroutine DeallocateVariables()
    implicit none
    deallocate( r, xini, xt )
  end subroutine DeallocateVariables

  subroutine FFTW_AllocateVariables()
    ! FFTW3:
    ! - Alocar usando fftw_alloc (ver página 88 de pdf)
    implicit none
    inptr   = fftw_alloc_real(int(insignalsize, C_SIZE_T))
    outptr  = fftw_alloc_complex(int(outsignalsize, C_SIZE_T))
    call c_f_pointer(inptr,   signalin,   [insignalsize])
    call c_f_pointer(outptr,  signalout,  [outsignalsize])
  end subroutine FFTW_AllocateVariables

  subroutine FFTW_DeallocateVariables()
    implicit none
    call fftw_free(inptr)
    call fftw_free(outptr)
  end subroutine FFTW_DeallocateVariables

  subroutine OpenFiles()
    implicit none
    integer   ::  ios
    open(unit=evolfile,   file="./output/evolution.dat",      iostat=ios, status="replace", action="write")
    if ( ios /= 0 ) stop "Error abriendo archivo de salida ./output/evolution.dat"
    open(unit=xinifile,   file="./output/parameter_xini.dat", iostat=ios, status="replace", action="write")
    if ( ios /= 0 ) stop "Error abriendo archivo de salida ./output/parameter_xini.dat"
    open(unit=rfile,      file="./output/parameter_r.dat",    iostat=ios, status="replace", action="write")
    if ( ios /= 0 ) stop "Error abriendo archivo de salida ./output/parameter_r.dat"
    open(unit=signalfile, file="./output/signalout.dat",      iostat=ios, status="replace", action="write")
    if ( ios /= 0 ) stop "Error abriendo archivo de salida ./output/signalout.dat"
    open(unit=histfile,   file="./output/histogram.dat",      iostat=ios, status="replace", action="write")
    if ( ios /= 0 ) stop "Error abriendo archivo de salida ./output/histogram.dat"
    open(unit=orbitsfile, file="./output/orbits.dat",         iostat=ios, status="replace", action="write")
    if ( ios /= 0 ) stop "Error abriendo archivo de salida ./output/orbits.dat"
    open(unit=zoomfile,   file="./output/orbits_zoom.dat",    iostat=ios, status="replace", action="write")
    if ( ios /= 0 ) stop "Error abriendo archivo de salida ./output/orbits_zoom.dat"
    open(unit=lyapfile,   file="./output/lyapunov.dat",       iostat=ios, status="replace", action="write")
    if ( ios /= 0 ) stop "Error abriendo archivo de salida ./output/lyapunov.dat"
  end subroutine OpenFiles

  subroutine CloseFiles()
    implicit none
    close(evolfile)
    close(rfile)
    close(evolfile)
    close(signalfile)
    close(histfile)
    close(orbitsfile)
    close(zoomfile)
    close(lyapfile)
  end subroutine CloseFiles

end module GlobalVariables
