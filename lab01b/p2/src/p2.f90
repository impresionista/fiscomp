program lab01b_p2

  use precision_interface
  use constantes, only: pi
  use GlobalVariables

  implicit none
  integer(ip)   ::  i, j, k
  real(rp)      ::  bin, bin_num, bin_top, hr, ri, xj, lambda, lambdaj


  ! Apartado (a) --------------------------------------------------------------
  ! Program parameters ------------------------------------
  Nr      = 5   !> Número de parámetros para mapeo logístico
  Nxini   = 4   !> Número de x iniciales para mapero logístico
  Nsteps  = 500
  call AllocateVariables
  r     = [ 1.5_rp, 3.3_rp, 3.5_rp, 3.55_rp, 4._rp ]
  xini  = [ 0.06_rp, 0.3_rp, 0.6_rp, 0.9_rp ]
  ! -------------------------------------------------------

  call OpenFiles

  do i = 1, Nxini
    write(xinifile,"(F16.2)") xini(i)
  end do
  do i = 1, Nr
    write(rfile,"(F16.2)") r(i)
  end do

  xt(:) = xini(:)
  ParameterSwep: do k = 1, Nr
    LogisticEvolution: do i = 1, Nsteps
      write(evolfile,*) i, xt
      call Logmap(xt, r(k))
    end do LogisticEvolution
    write(evolfile,*) i, xt
    write(evolfile,*)
    write(evolfile,*)
  end do ParameterSwep

  call DeallocateVariables
  ! ---------------------------------------------------------------------------


  ! Apartado (b) --------------------------------------------------------------
  ! Program parameters ------------------------------------
  Nr      = 5       !> Número de parámetros para mapeo logístico
  Nsteps  = 800     !> Número de puntos totales
  Ntrans  = 300     !> Números de puntos transitorios a descartar
  x0      = 0.6_rp  !> x inicial
  call AllocateVariables
  r       = [ 1.5_rp, 3.3_rp, 3.5_rp, 3.55_rp, 4._rp ]
  ! -------------------------------------------------------

  if (Ntrans .ge. Nsteps) stop "Error en parámetros para cálculo de fftw: Ntrans debe ser menor que Nsteps."

  Nsignal       = Nsteps-Ntrans
  insignalsize  = Nsignal
  outsignalsize = 1+Nsignal/2
  factor        = 2._rp*pi/real(outsignalsize,rp) !(tf-ti)

  call FFTW_AllocateVariables()

  plan_r2c = fftw_plan_dft_r2c_1d(Nsignal, signalin, signalout, FFTW_MEASURE)

  ParameterSwep_b: do i = 1, ubound(r(:), 1)

    xsignal = x0

    DiscardTransition_b: do k = 1, Ntrans
      call LogmapScalar(xsignal, r(i))
    end do DiscardTransition_b
    PopulateInSignal_b: do k = Ntrans+1, Nsteps
      call LogmapScalar(xsignal, r(i))
      signalin(k-Ntrans) = xsignal
    end do PopulateInSignal_b

    call fftw_execute_dft_r2c(plan_r2c, signalin, signalout)

    factor = 2._rp*pi/real(Nsignal,rp) !(tf-ti)
    NormalizeSignal_b: do j = -Nsignal/2, Nsignal/2
      if ( j < 0 ) then
        write(signalfile, *) factor*real(j,rp), abs(conjg( signalout(-j+1) )/real(Nsignal, rp))**2
      else
        write(signalfile, *) factor*real(j,rp), abs( signalout(j+1)/real(Nsignal, rp) )**2
      endif
    end do NormalizeSignal_b
    write(signalfile, *) ""
    write(signalfile, *) ""

  end do ParameterSwep_b

  call DeallocateVariables
  call FFTW_DeallocateVariables()
  call fftw_destroy_plan(plan_r2c)
  ! ---------------------------------------------------------------------------


  ! Apartado (c) --------------------------------------------------------------
  ! Program parameters ------------------------------------
  Nr      = 1       !> Número de parámetros para mapeo logístico
  Nsteps  = 10000   !> Número de puntos totales
  Ntrans  = 300     !> Números de puntos transitorios a descartar
  x0      = 0.6_rp  !> x inicial
  rmin    = 4._rp
  ! -------------------------------------------------------

  allocate(xini(Nsteps))

  xsignal = x0
  DiscardTransition_c: do k = 1, Ntrans
    call LogmapScalar(xsignal, rmin)
  end do DiscardTransition_c
  LogisticEvol_c: do k = 1, Nsteps
    call LogmapScalar(xsignal, rmin)
    xini(k) = xsignal
  end do LogisticEvol_c

  call mintomax(xini(:)) !> Ordeno el arrego de forma creciente

  ! Defino parametros para histograma
  ! bin_num = ceiling(sqrt(real(size(xini(:)),rp)))                     !> cantidad de bins
  bin_num = 20._rp                    !> cantidad de bins
  bin     = ( ceiling(maxval(xini(:))) - minval(xini(:)) ) / bin_num  !> ancho de bin

  allocate(histogram(int(bin_num),2)); histogram(:,:) = 0._rp

  ! Creo histograma
  bin_top = xini(1) + bin
  k = 1
  histogram(k,1) = xini(1)
  BinAssignment_c: do j = 1, size(xini(:))
    if (xini(j)<bin_top) then
      histogram(k,2) = histogram(k,2) + 1._rp
    else
      k = k+1
      bin_top = xini(1)+real(k,rp)*bin
      histogram(k,1) = bin_top
      histogram(k,2) = 1._rp
    end if
  end do BinAssignment_c

  histogram(:,2) = histogram(:,2)/real(size(xini(:)),rp)  !> Normalizo histograma

  do j = 1, size(histogram(:,1)), 1
    write(histfile,'(F20.3, F20.6)') histogram(j,:)
  end do

  deallocate(xini, histogram)
  ! ---------------------------------------------------------------------------


  ! Apartado (d) --------------------------------------------------------------
  ! Program parameters ------------------------------------
  Nsteps  = 600     !> Número de puntos totales
  Ntrans  = 300     !> Números de puntos transitorios a descartar
  Nr      = 200     !> Número de parámetros para mapeo logístico
  xmin    = 0.06_rp !> x inicial
  rmin    = 3.4_rp  !> r donde empezar el barrido
  rmax    = 4.0_rp  !> r donde terminar el barrido
  ! -------------------------------------------------------

  hr  = (rmax-rmin)/real(Nr, rp)

  Orbits_r_d: do i = 0, Nr
    xj  = xmin
    ri  = rmin + hr*real(i, rp)

    DiscardTransition_d: do j = 1, Ntrans
      call LogmapScalar(xj, ri)
    end do DiscardTransition_d

    LogisticEvolution_d: do j = Ntrans+1, Nsteps
      write(orbitsfile,*) ri, xj
      call LogmapScalar(xj, ri)
    end do LogisticEvolution_d
    write(orbitsfile,*) ri, xj
    write(orbitsfile,*)
    write(orbitsfile,*)

  end do Orbits_r_d


  ! Zoom region parameters --------------------------------
  ! Zoom en region 3.847 < r < 3.8568 para 0.13 < x < 0.185
  Nsteps    = 1200      !> Número de puntos totales
  Ntrans    = 300       !> Números de puntos transitorios a descartar
  Nr        = 200       !> Número de parámetros para mapeo logístico
  xmin      = 0.06_rp   !> x inicial
  xmin_zoom = 0.13_rp   !> x min para zoom
  xmax_zoom = 0.185_rp  !> x max para zoom
  rmin      = 3.847_rp  !> r donde empezar el barrido
  rmax      = 3.8568_rp !> r donde terminar el barrido
  ! -------------------------------------------------------

  hr  = (rmax-rmin)/real(Nr, rp)

  Orbits_r_Zoom: do i = 0, Nr

    xj  = xmin
    ri  = rmin + hr*real(i, rp)

    DiscardTransition_Zoom: do j = 1, Ntrans
      call LogmapScalar(xj, ri)
    end do DiscardTransition_Zoom

    Orbits_x_Zoom: do j = Ntrans+1, Nsteps

      Check_Zoom: if ( xj < xmin_zoom ) then
        exit Check_Zoom
      else if ( xmin_zoom < xj .AND. xj < xmax_zoom ) then
        write(zoomfile,*) ri, xj
      else if ( xmax_zoom < xj ) then
        exit Check_Zoom
      end if Check_Zoom

      call LogmapScalar(xj, ri)

    end do Orbits_x_Zoom

    write(zoomfile,*)
    write(zoomfile,*)

  end do Orbits_r_Zoom
  ! ---------------------------------------------------------------------------


  ! Apartado (e) --------------------------------------------------------------
  ! Program parameters ------------------------------------
  Ntrans    = 300     !> Nro de ptos a descartar antes de calcular orbitas
  Nsteps    = 1500    !> Nro de ptos para calcular el exponente
  xmin      = 0.06_rp
  Nr        = 200     !> Nro de ptos para barrer r
  rmin      = 2.0_rp  !> r donde empezar el barrido
  rmax      = 4.0_rp  !> r donde terminar el barrido
  ! -------------------------------------------------------

  hr  = (rmax-rmin)/real(Nr, rp)

  do i = 0, Nr

    xj      = xmin
    lambda  = 0._rp
    ri      = rmin + hr*real(i, rp)

    do j = 1, Ntrans
      call LogmapLyapunov(xj, ri, lambdaj)
      lambda = lambdaj + lambda
      call LogmapScalar(xj, ri)
    end do

    do j = 1, Nsteps
      call LogmapLyapunov(xj, ri, lambdaj)
      lambda = lambdaj + lambda
      call LogmapScalar(xj, ri)
    end do

    write(lyapfile,*) ri, lambda/real(Nsteps+Ntrans,rp)

  end do
  ! ---------------------------------------------------------------------------

  call CloseFiles

! -----------------------------------------------------------------------------
contains

  subroutine Logmap(x_inout, rr)
    implicit none
    real(rp), intent(inout) ::  x_inout(:)
    real(rp), intent(in)    ::  rr
    x_inout = rr*x_inout*(1-x_inout)
  end subroutine Logmap

  subroutine LogmapScalar(x_inout, rr)
    implicit none
    real(rp), intent(inout) ::  x_inout
    real(rp), intent(in)    ::  rr
    x_inout = rr*x_inout*(1-x_inout)
  end subroutine LogmapScalar

  subroutine LogmapLyapunov(xx, rr, lambdai)
    implicit none
    real(rp), intent(in)  :: rr, xx
    real(rp), intent(out) :: lambdai
      if ( abs(rr*(1._rp-2._rp*xx)) <= epsilon(1._rp) ) then
        lambdai=epsilon(1._rp)
        return
      else
        lambdai = log( abs( rr*(1._rp-2._rp*xx) ) )
        return
      endif
      return
  end subroutine LogmapLyapunov

  subroutine mintomax(A)
  ! ---------------------------------------------------------
  ! Ordena el arreglo unidimesional A de menor a mayor
  ! idea de: https://www.stackoverflow.com/questions/35324153/
  !                                        sorting-arrays-by-rows-in-fortran
  ! ---------------------------------------------------------
    implicit none
    real(rp), intent(inout)   :: A(:)
    real(rp)                  :: buf
    integer                   :: nsize, irow, krow

    nsize = size(A)

    do irow = 1, nsize
      krow = minloc(A(irow:nsize), dim=1) + irow - 1
      buf     = A(irow)
      A(irow) = A(krow)
      A(krow) = buf
    end do

    return
  end subroutine mintomax

end program lab01b_p2
