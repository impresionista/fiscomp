\babel@toc {spanish}{}\relax 
\contentsline {section}{\numberline {1}Compilación y ejecución}{1}{section.1}%
\contentsline {section}{\numberline {2}Problema 2: Sistemas Caóticos con ecuación logística}{3}{section.2}%
\contentsline {subsection}{\numberline {2.1}Apartado (a): Análisis de las trayectorias}{3}{subsection.2.1}%
\contentsline {subsection}{\numberline {2.2}Apartado (b): Análisis de espectro de potencias}{4}{subsection.2.2}%
\contentsline {subsection}{\numberline {2.3}Apartado (c): Análisis de histograma}{6}{subsection.2.3}%
\contentsline {subsection}{\numberline {2.4}Apartados (d) y (e): Diagrama de órbitas y exponente de Lyapunov}{6}{subsection.2.4}%
\contentsline {section}{\numberline {3}Problema 3: Caso del péndulo doble}{8}{section.3}%
\contentsline {subsection}{\numberline {3.1}Apartado (a): Análisis de trayectorias}{8}{subsection.3.1}%
\contentsline {subsection}{\numberline {3.2}Apartado (b): Secciones de Poincaré}{9}{subsection.3.2}%
\contentsline {subsection}{\numberline {3.3}Apartado (c): Espectro de potencias}{12}{subsection.3.3}%
\contentsline {subsection}{\numberline {3.4}Apartado (d): Mapa de Hey}{13}{subsection.3.4}%
