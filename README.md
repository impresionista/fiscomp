# Repositorio de trabajo para la materia 'Física Computacional' de la Facultad de Matemática, Astronomía, Física y Computación (FAMAF) de la Univesridad Nacional de Córdoba (UNC) de Argentina.

#### Toda consulta y aporte es bienvenido.

### Algunas referencias:

*  En el direcorio `lib` se encuentran módulos genéricos para distintas operaciones comunes como integración, derivación, evolución de ecuaciones diferenciales, generadores de números aleatorios, etc.

*  Cada módulo se encuentra lo más documentado que pude.

